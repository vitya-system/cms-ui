// Embed component widget.

document.addEventListener('cmsUiInit', function (e) {
    let embedEntityComponentWidgets = document.querySelectorAll('.embed-entity-component-widget.uninitialized');
    for (let embedEntityComponentWidget of embedEntityComponentWidgets) {
        embedEntityComponentWidget.classList.remove('uninitialized');
        embedEntityComponentWidget.addEventListener('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            if (event.target.classList.contains('embed-entity-component-widget__retrieve-info')) {
                event.target.classList.add('disabled');
                embedEntityComponentWidget.querySelector('.embed-entity-component-widget__spinner').classList.remove('d-none');
                fetch(event.target.getAttribute('href') + '?' + new URLSearchParams({
                        url: embedEntityComponentWidget.querySelector('.embed-entity-component-widget__source-url').value,
                    }).toString())
                    .then(response => response.json())
                    .then(data => {
                        if (data.status != 'ok') {
                            return;
                        }
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__title').value = data.content.title;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__description').value = data.content.description;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__url').value = data.content.url;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__keywords').value = data.content.keywords;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__image').value = data.content.image;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__html').value = data.content.html;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__width').value = data.content.width;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__height').value = data.content.height;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__ratio').value = data.content.ratio;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__author-name').value = data.content.author_name;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__author-url').value = data.content.author_url;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__cms').value = data.content.cms;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__language').value = data.content.language;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__languages').value = data.content.languages;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__provider-name').value = data.content.provider_name;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__provider-url').value = data.content.provider_url;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__icon').value = data.content.icon;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__favicon').value = data.content.favicon;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__published-time').value = data.content.published_time;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__license').value = data.content.license;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__feeds').value = data.content.feeds;
                    })
                    .catch(err => console.log(err))
                    .finally(() => {
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__spinner').classList.add('d-none');
                        event.target.classList.remove('disabled');
                    });
            }
        });
    }
});
