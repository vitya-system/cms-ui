// UI initialization.

const cmsUiInit = new Event('cmsUiInit');

window.addEventListener('load', (event) => {
    document.dispatchEvent(cmsUiInit);
});
