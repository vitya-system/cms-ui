// Composite list.

document.addEventListener('cmsUiInit', function (e) {
    // Add block.
    let editionFormWidgets = document.querySelectorAll('.composite-list-edition-form-widget.uninitialized');
    for (let editionFormWidget of editionFormWidgets) {
        editionFormWidget.classList.remove('uninitialized');
        let editionFormWidgetAddBlockLinks = editionFormWidget.querySelector('.composite-list-edition-form-widget__add-block-links').querySelectorAll('.composite-list-edition-form-widget__add-block-link');
        for (let editionFormWidgetAddBlockLink of editionFormWidgetAddBlockLinks) {
            editionFormWidgetAddBlockLink.addEventListener('click', async function (e) {
                e.preventDefault();
                let response = await fetch(this.getAttribute('href'), {
                    method: 'GET'
                });
                if (response.ok) {
                    let template = document.createElement('template');
                    template.innerHTML = await response.text();
                    console.log(template.innerHTML);
                    if (template.content.childNodes.length >= 1) {
                        let newBlock = template.content.firstChild;
                        editionFormWidget.querySelector('.composite-list-edition-form-widget__block-list').appendChild(newBlock);
                        document.dispatchEvent(new Event('cmsUiInit'));
                    }
                }
            });
        }
        let editionFormWidgetBlockList = editionFormWidget.querySelector('.composite-list-edition-form-widget__block-list');
        new Sortable(editionFormWidgetBlockList, {
            handle: '.composite-list-edition-form-widget__block-handle',
            draggable: '.composite-list-edition-form-widget__block',
            animation: 150
        });
    }
    // Delete block.
    let editionFormWidgetBlocks = document.querySelectorAll('.composite-list-edition-form-widget__block.uninitialized');
    for (let editionFormWidgetBlock of editionFormWidgetBlocks) {
        editionFormWidgetBlock.classList.remove('uninitialized');
        let editionFormWidgetDelete = editionFormWidgetBlock.querySelector('.composite-list-edition-form-widget__delete');
        editionFormWidgetDelete.addEventListener('click', function (e) {
            editionFormWidgetBlock.parentNode.removeChild(editionFormWidgetBlock);
        });
    }
});
