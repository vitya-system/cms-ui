// Tree index controller.

document.addEventListener('cmsUiInit', function (e) {
    let treeStructureWidgets = document.querySelectorAll('.tree-structure-widget.uninitialized');
    for (let treeStructureWidget of treeStructureWidgets) {
        treeStructureWidget.classList.remove('uninitialized');
        let tree = new SortableTree({
            nodes: JSON.parse(treeStructureWidget.querySelector('.tree-structure-widget__json').textContent),
            element: treeStructureWidget.querySelector('.tree-structure-widget__tree'),
            lockRootLevel: false,
            renderLabel: (data) => {
                return data.item_html;
            },
            onChange: ({nodes, movedNode, srcParentNode, targetParentNode}) => {
                let newAssociativeArray = [];
                let alreadyAddedNodes = [];
                Object.values(tree.nodeCollection).forEach(function(value, key) {
                    let parentId = parseInt(value._data.id);
                    if (!alreadyAddedNodes.includes(parentId)) {
                        newAssociativeArray.push([parentId, 0]);
                        alreadyAddedNodes.push(parentId);
                    }
                    value.subnodesData.forEach(function(subnodeData) {
                        let childId = parseInt(subnodeData.id);
                        if (!alreadyAddedNodes.includes(childId)) {
                            newAssociativeArray.push([childId, parentId]);
                            alreadyAddedNodes.push(childId);
                        }
                    });
                });
                treeStructureWidget.querySelector('.tree-structure-widget__associative-array').value = JSON.stringify(newAssociativeArray, 4);
            }
        });
    }
});
