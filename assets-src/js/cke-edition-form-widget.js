// CKEditor.

document.addEventListener('cmsUiInit', function (e) {
    let editionFormWidgets = document.querySelectorAll('.cke-edition-form-widget.uninitialized');
    for (let editionFormWidget of editionFormWidgets) {
        editionFormWidget.classList.remove('uninitialized');
        let editionFormWidgetTextarea = editionFormWidget.querySelector('textarea');
        ClassicEditor
            .create(
                editionFormWidgetTextarea, 
                {
                    toolbar: {
                        items: ['heading', '|', 'bold', 'italic', '|', 'undo', 'redo', '|', 'numberedList', 'bulletedList']
                    }
                }
            )
            .then(editor => {
                if (editionFormWidgetTextarea.disabled) {
                    editor.enableReadOnlyMode('datum-permission');
                }
            })
            .catch(error => {
                console.error(error.stack);
            })
        ;
    }
});
