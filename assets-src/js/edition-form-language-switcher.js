// Edition form language switcher.

class CmsUiLocaleSwitcher {

    #localeSwitcherDomElement;

    constructor(localeSwitcherDomElement) {
        this.#localeSwitcherDomElement = localeSwitcherDomElement;
    }

    init() {
        if (false === this.#localeSwitcherDomElement.classList.contains('uninitialized')) {
            return;
        }
        this.#localeSwitcherDomElement.classList.remove('uninitialized');
        this.update();
        let that = this;
        document.addEventListener('cmsUiInit', function (e) {
            that.update();
        });
        let localeCheckboxes = this.#localeSwitcherDomElement.querySelectorAll('.language-switcher__checkbox');
        for (let localeCheckbox of localeCheckboxes) {
            localeCheckbox.addEventListener('change', function (e) {
                that.update();
            });
        }
    }

    update() {
        let localeCheckboxes = this.#localeSwitcherDomElement.querySelectorAll('.language-switcher__checkbox');
        let localeContainers = document.querySelectorAll('.locale-container');
        for (let localeCheckbox of localeCheckboxes) {
            for (let localeContainer of localeContainers) {
                if (localeContainer.dataset.locale === localeCheckbox.dataset.locale) {
                    if (localeCheckbox.checked) {
                        localeContainer.hidden = false;
                    } else {
                        localeContainer.hidden = true;
                    }
                }
            }
        }
    }

}

document.addEventListener('cmsUiInit', function (e) {
    let localeSwitcherDomElements = document.querySelectorAll('.language-switcher.uninitialized');
    for (let localeSwitcherDomElement of localeSwitcherDomElements) {
        let localeSwitcher = new CmsUiLocaleSwitcher(localeSwitcherDomElement);
        localeSwitcher.init();
    }
});
