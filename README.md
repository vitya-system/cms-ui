# Vitya PHP CMS framexork

This package contains the full Vitya CMS framework, allowing you to build a
complete web application with a public interface and an admin interface.

It's designed with a few guidelines in mind:
* It must be easy to understand.
* It must work well with multilingual content.
* It must be easy to extend.

You can try Vitya CMS easily using
[our project template](https://gitlab.com/vitya-system/cms-template).
