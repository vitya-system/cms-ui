<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\EntityCmsAdminIntegration;

use Psr\Http\Message\UriInterface;

class UserEntityCmsAdminIntegration extends EntityCmsAdminIntegration
{
    public function getName(): string
    {
        return 'user (' . $this->getEntity()->getMachineName() . ')';
    }

    public function getCollectionName(): string
    {
        return 'users (' . $this->getEntity()->getCollectionMachineName() . ')';
    }

    public function getSupportedEntityClass(): string
    {
        return 'Vitya\CmsComponent\Entity\AbstractUserEntity';
    }

    public function getEditionFormWidgets(?UriInterface $origin_uri = null): array
    {
        $entity = $this->getEntity();
        $user = $this->getAuthenticationService()->getUser('cms');
        $edition_form_zones = parent::getEditionFormWidgets($origin_uri);
        $user_management_component_widget = $this->getDependencyInjector()
            ->make('Vitya\CmsApplication\Widget\UserManagementEntityComponentWidget')
            ->setEntityComponent($entity->getComponent('user_management'))
            ->setUser($user)
        ;
        if (null !== $origin_uri) {
            $user_management_component_widget->setOriginUri($origin_uri);
        }
        $first_index = array_keys($edition_form_zones['main'])[0];
        array_unshift($edition_form_zones['main'][$first_index], $user_management_component_widget);
        $identity_component_widget = $this->getDependencyInjector()
            ->make('Vitya\CmsApplication\Widget\IdentityEntityComponentWidget')
            ->setEntityComponent($entity->getComponent('identity'))
            ->setUser($user)
        ;
        if (null !== $origin_uri) {
            $identity_component_widget->setOriginUri($origin_uri);
        }
        $first_index = array_keys($edition_form_zones['main'])[0];
        array_unshift($edition_form_zones['main'][$first_index], $identity_component_widget);
        return $edition_form_zones;
    }

}
