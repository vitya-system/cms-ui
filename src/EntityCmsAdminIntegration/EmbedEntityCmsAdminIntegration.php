<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\EntityCmsAdminIntegration;

use Psr\Http\Message\UriInterface;

class EmbedEntityCmsAdminIntegration extends EntityCmsAdminIntegration
{
    public function getName(): string
    {
        return 'embed (' . $this->getEntity()->getMachineName() . ')';
    }

    public function getCollectionName(): string
    {
        return 'embeds (' . $this->getEntity()->getCollectionMachineName() . ')';
    }

    public function getSupportedEntityClass(): string
    {
        return 'Vitya\CmsComponent\Entity\AbstractEmbedEntity';
    }

    public function getEditionFormWidgets(?UriInterface $origin_uri = null): array
    {
        $entity = $this->getEntity();
        $user = $this->getAuthenticationService()->getUser('cms');
        $edition_form_zones = parent::getEditionFormWidgets($origin_uri);
        if ($entity->getComponent('acl')->canBeViewed($user)) {
            $acl_component_widget = $this->getDependencyInjector()
                ->make('Vitya\CmsApplication\Widget\AclEntityComponentWidget')
                ->setEntityComponent($entity->getComponent('acl'))
                ->setUser($user)
                ->setPermissionLabels($this->getAclPermissionLabels())
            ;
            if (null !== $origin_uri) {
                $acl_component_widget->setOriginUri($origin_uri);
            }
            $edition_form_zones['aside'][] = [
                $acl_component_widget
            ];
        }
        $embed_component_widget = $this->getDependencyInjector()
            ->make('Vitya\CmsApplication\Widget\EmbedEntityComponentWidget')
            ->setEntityComponent($entity->getComponent('embed'))
            ->setUser($user)
        ;
        $first_index = array_keys($edition_form_zones['main'])[0];
        array_unshift($edition_form_zones['main'][$first_index], $embed_component_widget);
        return $edition_form_zones;
    }

}
