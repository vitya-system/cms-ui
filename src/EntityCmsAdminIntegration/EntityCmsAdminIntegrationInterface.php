<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\EntityCmsAdminIntegration;

use Psr\Http\Message\UriInterface;
use Vitya\CmsComponent\Entity\EntityInterface;
use Vitya\CmsApplication\Widget\EntityCardWidget;

interface EntityCmsAdminIntegrationInterface
{
    public function getEntity(): EntityInterface;

    public function setEntity(EntityInterface $entity): static;

    public function getName(): string;

    public function getCollectionName(): string;

    public function getTitle(string $locale): string;

    public function getEntryPointUri(): ?UriInterface;

    public function getIndexUri(): ?UriInterface;

    public function getProcessIndexUri(): ?UriInterface;

    public function getProcessCreationFormUri(): ?UriInterface;

    public function getEditionFormUri(?UriInterface $origin_uri = null): ?UriInterface;

    public function getProcessEditionFormUri(): ?UriInterface;

    public function getDeletionFormUri(?UriInterface $origin_uri = null): ?UriInterface;

    public function getProcessDeletionFormUri(): ?UriInterface;

    public function getDuplicationFormUri(?UriInterface $origin_uri = null): ?UriInterface;

    public function getProcessDuplicationFormUri(): ?UriInterface;

    public function makeCardWidget(): EntityCardWidget;

}
