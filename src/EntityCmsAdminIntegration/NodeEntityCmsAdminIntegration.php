<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\EntityCmsAdminIntegration;

use Psr\Http\Message\UriInterface;

class NodeEntityCmsAdminIntegration extends EntityCmsAdminIntegration
{
    public function getName(): string
    {
        return 'node (' . $this->getEntity()->getMachineName() . ')';
    }

    public function getCollectionName(): string
    {
        return 'nodes (' . $this->getEntity()->getCollectionMachineName() . ')';
    }

    public function getSupportedEntityClass(): string
    {
        return 'Vitya\CmsComponent\Entity\AbstractNodeEntity';
    }

    public function getIndexUri(): ?UriInterface
    {
        return $this->getRouter()->createUri(
            'cms-ui-entity-tree-index',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
            ]
        );
    }

    public function getProcessIndexUri(): ?UriInterface
    {
        return $this->getRouter()->createUri(
            'cms-ui-process-entity-tree-index',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
            ]
        );
    }

    public function getEditionFormWidgets(?UriInterface $origin_uri = null): array
    {
        $entity = $this->getEntity();
        $user = $this->getAuthenticationService()->getUser('cms');
        $edition_form_zones = parent::getEditionFormWidgets($origin_uri);
        if ($entity->getComponent('acl')->canBeViewed($user)) {
            $acl_component_widget = $this->getDependencyInjector()
                ->make('Vitya\CmsApplication\Widget\AclEntityComponentWidget')
                ->setEntityComponent($entity->getComponent('acl'))
                ->setUser($user)
                ->setPermissionLabels($this->getAclPermissionLabels())
            ;
            if (null !== $origin_uri) {
                $acl_component_widget->setOriginUri($origin_uri);
            }
            $edition_form_zones['aside'][] = [
                $acl_component_widget
            ];
        }
        return $edition_form_zones;
    }

}
