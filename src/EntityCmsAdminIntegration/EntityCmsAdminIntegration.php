<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\EntityCmsAdminIntegration;

use Exception;
use Psr\Http\Message\UriInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsComponent\Entity\EntityInterface;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\DisplayBoxFilterWidget\DisplayBoxFilterWidgetInterface;
use Vitya\CmsApplication\DisplayBoxSortOption\DisplayBoxSortOptionInterface;
use Vitya\CmsApplication\Widget\EntityCardWidget;
use Vitya\CmsApplication\Widget\EntityWidgetBuilder;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Route\RouterInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityCmsAdminIntegration implements EntityCmsAdminIntegrationInterface
{
    private $authn = null;
    private $cmsUi = null;
    private $dependencyInjector = null;
    private $entityWidgetBuilder = null;
    private $entity = null;
    private $localeCatalog = null;
    private $router = null;

    public function __construct(
        AuthenticationService $authn,
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        EntityWidgetBuilder $entity_widget_builder,
        LocaleCatalog $locale_catalog,
        RouterInterface $router
    )
    {
        $this->authn = $authn;
        $this->cmsUi = $cms_ui;
        $this->dependencyInjector = $dependency_injector;
        $this->entityWidgetBuilder = $entity_widget_builder;
        $this->localeCatalog = $locale_catalog;
        $this->router = $router;
    }

    public function getAuthenticationService(): AuthenticationService
    {
        return $this->authn;
    }

    public function getCmsUi(): CmsUi
    {
        return $this->cmsUi;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getEntity(): AbstractEntity
    {
        if (null === $this->entity) {
            throw new Exception('No entity set yet.');
        }
        return $this->entity;
    }

    public function getEntityWidgetBuilder(): EntityWidgetBuilder
    {
        return $this->entityWidgetBuilder;
    }

    public function getLocaleCatalog(): LocaleCatalog
    {
        return $this->localeCatalog;
    }

    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    public function setEntity(EntityInterface $entity): static
    {
        if (false === $entity instanceof ($this->getSupportedEntityClass())) {
            throw new Exception(''
                . 'This type of entity is not supported by this CmsAdminIntegration. '
                . 'Expected: ' . $this->getSupportedEntityClass() . '. '
                . 'Given: ' . get_class($entity) . '.');
        }
        $this->entity = $entity;
        return $this;
    }

    public function getName(): string
    {
        return 'entity (' . $this->getEntity()->getMachineName() . ')';
    }

    public function getCollectionName(): string
    {
        return 'entitties (' . $this->getEntity()->getCollectionMachineName() . ')';
    }

    public function getSupportedEntityClass(): string
    {
        return 'Vitya\CmsComponent\Entity\AbstractEntity';
    }

    public function getTitle(?string $locale = null): string
    {
        return $this->getEntity()->getTitle($locale);
    }

    public function getEntryPointUri(): ?UriInterface
    {
        return $this->getIndexUri();
    }

    public function getIndexUri(): ?UriInterface
    {
        return $this->getRouter()->createUri(
            'cms-ui-entity-index',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
            ]
        );
    }

    public function getProcessIndexUri(): ?UriInterface
    {
        return null;
    }

    public function getProcessCreationFormUri(): ?UriInterface
    {
        return $this->getRouter()->createUri(
            'cms-ui-process-entity-creation-form',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
            ]
        );
    }

    public function getEditionFormUri(?UriInterface $origin_uri = null): ?UriInterface
    {
        $query_string_parameters = [];
        if (null !== $origin_uri) {
            $query_string_parameters['origin_url'] = (string) $origin_uri;
        }
        return $this->getRouter()->createUri(
            'cms-ui-entity-edition-form',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
                'id' => (string) $this->getEntity()->getId(),
            ],
            $query_string_parameters
        );
    }

    public function getProcessEditionFormUri(): ?UriInterface
    {
        return $this->getRouter()->createUri(
            'cms-ui-process-entity-edition-form',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
                'id' => (string) $this->getEntity()->getId(),
            ]
        );
    }

    public function getDeletionFormUri(?UriInterface $origin_uri = null): ?UriInterface
    {
        $query_string_parameters = [];
        if (null !== $origin_uri) {
            $query_string_parameters['origin_url'] = (string) $origin_uri;
        }
        return $this->getRouter()->createUri(
            'cms-ui-entity-deletion-form',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
                'id' => (string) $this->getEntity()->getId(),
            ],
            $query_string_parameters
        );
    }

    public function getProcessDeletionFormUri(): ?UriInterface
    {
        return $this->getRouter()->createUri(
            'cms-ui-process-entity-deletion-form',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
                'id' => (string) $this->getEntity()->getId(),
            ]
        );
    }

    public function getDuplicationFormUri(?UriInterface $origin_uri = null): ?UriInterface
    {
        $query_string_parameters = [];
        if (null !== $origin_uri) {
            $query_string_parameters['origin_url'] = (string) $origin_uri;
        }
        return $this->getRouter()->createUri(
            'cms-ui-entity-duplication-form',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
                'id' => (string) $this->getEntity()->getId(),
            ],
            $query_string_parameters
        );
    }

    public function getProcessDuplicationFormUri(): ?UriInterface
    {
        return $this->getRouter()->createUri(
            'cms-ui-process-entity-duplication-form',
            [
                'collection_slug' => $this->getEntity()->getCollectionMachineName(),
                'id' => (string) $this->getEntity()->getId(),
            ]
        );
    }

    public function makeCardWidget(): EntityCardWidget
    {
        $widget = $this->getDependencyInjector()->make('Vitya\CmsApplication\Widget\EntityCardWidget');
        $widget->setEntityCmsAdminIntegration($this);
        return $widget;
    }

    public function getAclPermissionLabels(): array
    {
        return [
            AbstractEntity::PERMISSION_VIEW => 'View',
            AbstractEntity::PERMISSION_MODIFY => 'Modify',
            AbstractEntity::PERMISSION_MODIFY_ACL => 'Change permissions',
        ];
    }

    public function getEditionFormMainZoneDescription(): array
    {
        return [];
    }

    public function getEditionFormAsideZoneDescription(): array
    {
        return [];
    }

    public function getEditionFormWidgets(?UriInterface $origin_uri = null): array
    {
        $entity = $this->getEntity();
        $user = $this->getAuthenticationService()->getUser('cms');
        $edition_form_zones = [
            'main' => [],
            'aside' => [],
        ];
        $edition_form_zones['main'][] = $this->getEntityWidgetBuilder()->createWidgetsFromDescription($this->getEditionFormMainZoneDescription(), $entity->getRootComponent(), $user, $origin_uri);
        $edition_form_zones['aside'][] = $this->getEntityWidgetBuilder()->createWidgetsFromDescription($this->getEditionFormAsideZoneDescription(), $entity->getRootComponent(), $user, $origin_uri);
        $edition_form_zones = $this->addEditionFormPublicationWidget($edition_form_zones, $entity, $user, $origin_uri);
        $widget_list = [];
        foreach ($edition_form_zones as $edition_form_zone) {
            foreach ($edition_form_zone as $edition_form_subzone) {
                foreach ($edition_form_subzone as $widget) {
                    $widget_list[] = $widget;
                    foreach ($widget->getDescendants() as $descendant_widget) {
                        $widget_list[] = $descendant_widget;
                    }
                }
            }
        }
        $counter = 0;
        foreach ($widget_list as $widget) {
            $widget->setBaseName($this->getEntityWidgetBuilder()->generateBaseName($entity->getAddress(), $counter++));
        }
        return $edition_form_zones;
    }

    public function addEditionFormPublicationWidget(array $edition_form_zones, AbstractEntity $entity, UserInterface $user, UriInterface $origin_uri = null): array
    {
        $entity_options = $entity->getOptions();
        if ($entity_options['use_publication']) {
            $publication_component_widget = $this->getDependencyInjector()
                ->make('Vitya\CmsApplication\Widget\PublicationEntityComponentWidget')
                ->setUser($user)
            ;
            $locales = $this->localeCatalog->getLocales();
            foreach ($locales as $locale) {
                $publication_component_widget->addEntityComponent(
                    $entity->getComponent('publication')->getChild($locale->getId()), $locale->getId(), $locale->getName()
                );
            }
            $edition_form_zones['aside'][] = [
                $publication_component_widget
            ];
        }
        return $edition_form_zones;
    }

    public function getSortOptionsDescription(): array
    {
        return [
            'creation_time' => [
                'class' => 'Vitya\CmsApplication\DisplayBoxSortOption\CreationTimeDisplayBoxSortOption',
                'options' => [],
            ],
            'last_modification_time' => [
                'class' => 'Vitya\CmsApplication\DisplayBoxSortOption\LastModificationTimeDisplayBoxSortOption',
                'options' => [],
            ],
        ];
    }

    public function getSanitizeDisplayBoxSortOptionsDescription(array $description): array
    {
        foreach ($description as $k => $sort_option_description) {
            if (false === isset($description[$k]['class']) || false === is_string($description[$k]['class'])) {
                throw new Exception('A display box sort option class name must be provided (element "' . $k . '" of the description).');
            }
            if (false === isset($description[$k]['options'])) {
                $description[$k]['options'] = [];
            }
        }
        return $description;
    }

    public function getDisplayBoxSortOptions(AbstractEntity $entity, UserInterface $user): array
    {
        $sort_options = [];
        $description = $this->getSanitizeDisplayBoxSortOptionsDescription($this->getSortOptionsDescription());
        foreach ($description as $k => $sort_option_description) {
            $sort_option = $this->dependencyInjector->make($sort_option_description['class']);
            if (false === $sort_option instanceof DisplayBoxSortOptionInterface) {
                throw new Exception(get_class($$sort_option) . ' does not implement DisplayBoxSortOptionInterface.');
            }
            $sort_option
                ->setId($k)
                ->setEntity($entity)
                ->setUser($user)
                ->setOptions($sort_option_description['options'])
            ;
            $sort_options[] = $sort_option;
        }
        return $sort_options;
    }

    public function getFilterWidgetsDescription(): array
    {
        $entity_model = $this->getEntity();
        $entity_options = $entity_model->getOptions();
        $description = array();
        if ($entity_options['use_publication']) {
            $description['publication'] = [
                'class' => 'Vitya\CmsApplication\DisplayBoxFilterWidget\PublicationDisplayBoxFilterWidget',
                'options' => [
                    'component' => 'publication/' . $this->getCmsUi()->getWorkingLocale()->getId(),
                ],
            ];
        }
        return $description;
    }

    public function getSanitizeDisplayBoxFilterWidgetsDescription(array $description): array
    {
        foreach ($description as $k => $filter_widget_description) {
            if (false === isset($description[$k]['class']) || false === is_string($description[$k]['class'])) {
                throw new Exception('A display box filter widget class name must be provided (element "' . $k . '" of the description).');
            }
            if (false === isset($description[$k]['options'])) {
                $description[$k]['options'] = [];
            }
        }
        return $description;
    } 

    public function getDisplayBoxFilterWidgets(AbstractEntity $entity, UserInterface $user): array
    {
        $filter_widgets = [];
        $description = $this->getSanitizeDisplayBoxFilterWidgetsDescription($this->getFilterWidgetsDescription());
        foreach ($description as $k => $filter_widget_description) {
            $filter_widget = $this->getDependencyInjector()->make($filter_widget_description['class']);
            if (false === $filter_widget instanceof DisplayBoxFilterWidgetInterface) {
                throw new Exception(get_class($$filter_widget) . ' does not implement DisplayBoxFilterWidgetInterface.');
            }
            $filter_widget
                ->setId($k)
                ->setEntity($entity)
                ->setUser($user)
                ->setOptions($filter_widget_description['options'])
            ;
            $filter_widgets[] = $filter_widget;
        }
        return $filter_widgets;
    }

    public function giveUniqueBaseNameToDisplayBoxFilterWidgets(string $prefix, array $filter_widgets): void
    {
        $counter = 0;
        foreach ($filter_widgets as $widget) {
            $widget->setBaseName(md5($prefix . '+' . $counter++));
        }
    }

}
