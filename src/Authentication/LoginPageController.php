<?php

/*
 * Copyright 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Authentication;

use Psr\Http\Message\ResponseInterface;
use Vitya\Application\Controller\HtmlPageTrait;
use Vitya\Component\Authentication\LoginPageAuthenticator;
use Vitya\Component\Session\SessionService;
use Vitya\CmsApplication\Controller\CmsUiPageController;

class LoginPageController extends CmsUiPageController
{
    use HtmlPageTrait;

    public function loginPage(
        SessionService $session_service,
        string $realm
    ): ResponseInterface {
        $error = (int) $session_service->getSession()->pop('authentication__login_page_' . $realm . '_last_error', LoginPageAuthenticator::OK);
        $last_username = (string) $session_service->getSession()->pop('authentication__login_page_' . $realm . '_last_username', '');
        $error_message = '';
        switch ($error) {
            case LoginPageAuthenticator::OK:
                $error_message = '';
                break;
            case LoginPageAuthenticator::ERROR_INVALID_USERNAME_OR_PASSWORD:
                $error_message = 'Invalid username or password.';
                break;
            default:
                $error_message = 'An error occurred during the authentication process.';   
        }
        $this->setTitle('Login');
        return $this->render(
            '@CmsUi/Authentication/login.twig',
            [
                'realm' => $realm,
                'error_message' => $error_message,
                'last_username' => $last_username,
            ]
        );    
    }

}
