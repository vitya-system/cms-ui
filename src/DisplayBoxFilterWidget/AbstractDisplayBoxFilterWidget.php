<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DisplayBoxFilterWidget;

use Exception;
use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Frontend\WebFrontend;

abstract class AbstractDisplayBoxFilterWidget implements DisplayBoxFilterWidgetInterface
{
    private $baseName = 'unnamed';
    private $twig;
    private $id = '';
    private $entity = null;
    private $options = [];
    private $user = null;
    private $webFrontend = null;

    public function __construct(Environment $twig, WebFrontend $web_frontend)
    {
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    abstract public function render(): string;

    public function getBaseName(): string
    {
        return $this->baseName;
    }

    public function setBaseName(string $base_name): static
    {
        if (false === preg_match('/^[a-z0-9\-]+$/', $base_name)) {
            throw new Exception('Invalid base name for widget.');
        }
        $this->baseName = $base_name;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function setEntity(AbstractEntity $entity): static
    {
        $this->entity = $entity;
        return $this;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): static
    {
        $options = array_merge($this->getDefaultOptions(), $options);
        $this->assertValidOptions($options);
        $this->options = $options;
        return $this;
    }

    public function getEntity(): AbstractEntity
    {
        if (null === $this->entity) {
            throw new Exception('No entity attached to this display box filter widget.');
        }
        return $this->entity;
    }

    public function getUser(): UserInterface
    {
        if (null === $this->user) {
            throw new Exception('No user attached to this display box filter widget.');
        }
        return $this->user;
    }

    public function getDefaultOptions(): array
    {
        return [];
    }

    public function assertValidOptions(array $options): static
    {
        return $this;
    }

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface
    {
        return $uri;
    }

    public function getEntityListModifiers(): array
    {
        return [];
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getWebFrontend(): WebFrontend
    {
        return $this->webFrontend;
    }

}
