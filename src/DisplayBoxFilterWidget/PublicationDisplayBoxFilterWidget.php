<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DisplayBoxFilterWidget;

use Exception;
use Psr\Http\Message\UriInterface;
use Vitya\CmsComponent\EntityListModifier\PublicationFilter;

class PublicationDisplayBoxFilterWidget extends AbstractDisplayBoxFilterWidget
{
    public function render(): string
    {
        $options = $this->getOptions();
        return $this->getTwig()->render(
            '@CmsUi/DisplayBoxFilterWidget/PublicationDisplayBoxFilterWidget/widget.twig',
            [
                'base_name' => $this->getBaseName(),
                'label' => $options['label'],
                'selected_option' => $this->getSelectedOption(),
            ]
        );
    }

    public function getDefaultOptions(): array
    {
        $default_options = [
            'label' => 'Publication status',
            'component' => '',
        ];
        return array_merge(parent::getDefaultOptions(), $default_options);
    }

    public function assertValidOptions(array $options): static
    {
        parent::assertValidOptions($options);
        if ((false === isset($options['label'])) || (false === is_string($options['label']))) {
            throw new Exception('PublicationDisplayBoxFilterWidget "label" option must be defined as a string.');
        }
        if ((false === isset($options['component'])) || (false === is_string($options['component']))) {
            throw new Exception('PublicationDisplayBoxFilterWidget "component" option must be defined as a string.');
        }
        return $this;
    }

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface
    {
        if (null === $uri) {
            return null;
        }
        parse_str($uri->getQuery(), $query_params);
        $query_params[$this->getBaseName()] = null;
        if ('' !== $this->getSelectedOption()) {
            $query_params[$this->getBaseName()] = $this->getSelectedOption();
        }
        $uri = $uri->withQuery(http_build_query($query_params));
        return $uri;
    }

    public function getEntityListModifiers(): array
    {
        $component_relative_address = $this->getOptions()['component'];
        if ('currently_published' === $this->getSelectedOption()) {
            return [new PublicationFilter($component_relative_address, 'currently_published')];
        }
        if ('currently_not_published' === $this->getSelectedOption()) {
            return [new PublicationFilter($component_relative_address, 'currently_not_published')];
        }
        if ('to_be_published' === $this->getSelectedOption()) {
            return [new PublicationFilter($component_relative_address, 'to_be_published')];
        }
        if ('previously_published' === $this->getSelectedOption()) {
            return [new PublicationFilter($component_relative_address, 'previously_published')];
        }
        if ('publishable' === $this->getSelectedOption()) {
            return [new PublicationFilter($component_relative_address, 'publishable')];
        }
        if ('not_publishable' === $this->getSelectedOption()) {
            return [new PublicationFilter($component_relative_address, 'not_publishable')];
        }
        return [];
    }

    public function getSelectedOption(): string
    {
        $query_string_params = $this->getWebFrontend()->getMainServerRequest()->getQueryParams();
        if (false === isset($query_string_params[$this->getBaseName()])) {
            return '';
        }
        $selected_option = (string) $query_string_params[$this->getBaseName()];
        if (false === in_array($selected_option, ['currently_published', 'currently_not_published', 'to_be_published', 'previously_published', 'publishable', 'not_publishable'])) {
            return '';
        }
        return $selected_option;
    }

}
