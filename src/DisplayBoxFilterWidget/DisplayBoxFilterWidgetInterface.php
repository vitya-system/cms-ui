<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DisplayBoxFilterWidget;

use Psr\Http\Message\UriInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Widget\WidgetInterface;

interface DisplayBoxFilterWidgetInterface extends WidgetInterface
{
    public function getBaseName(): string;

    public function setBaseName(string $base_name): static;

    public function getId(): string;

    public function setId(string $id): static;

    public function setEntity(AbstractEntity $entity): static;

    public function setUser(UserInterface $user): static;

    public function getOptions(): array;

    public function setOptions(array $options): static;

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface;

    public function getEntityListModifiers(): array;

}
