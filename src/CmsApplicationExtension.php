<?php

/*
 * Copyright 2020, 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication;

use Vitya\Application\Application;
use Vitya\Application\ApplicationExtensionInterface;
use Vitya\Application\ServiceProvider\AuthenticationServiceProvider;
use Vitya\Application\ServiceProvider\ImageFactoryServiceProvider;
use Vitya\Application\ServiceProvider\MailerServiceProvider;
use Vitya\Application\ServiceProvider\SessionServiceProvider;
use Vitya\Application\ServiceProvider\TwigServiceProvider;
use Vitya\CmsComponent\ServiceProvider\NonceServiceProvider;
use Vitya\CmsComponent\ServiceProvider\TwigServiceExtender;
use Vitya\CmsApplication\ServiceProvider\CmsUiServiceProvider;
use Vitya\CmsApplication\ServiceProvider\AssetManagerServiceExtender;
use Vitya\CmsApplication\ServiceProvider\EntityWidgetBuilderServiceProvider;
use Vitya\Component\Authentication\LoginPageAuthenticator;

class CmsApplicationExtension implements ApplicationExtensionInterface
{
    private $app = null;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $app->mergeConfig( [
            'cms_ui__url_path_prefix' => '/admin',
        ]);
    }

    public function __toString(): string
    {
        return 'Vitya CMS Admin UI Extension';
    }

    public function parametrize(): void
    {
        $this->app['cms_ui__url_path_prefix'] = (string) $this->app['config']['cms_ui__url_path_prefix'];
        // Router service parameters.
        $this->app->loadRoutesFromJsonFile(realpath(__DIR__ . '/../routes.json'));
        $this->app->loadRoutesFromJsonFile(realpath(__DIR__ . '/../cms-ui-routes.json'), $this->app['cms_ui__url_path_prefix']);
        // Register services.
        $this->app->register(new AuthenticationServiceProvider());
        $this->app->register(new CmsUiServiceProvider());
        $this->app->register(new EntityWidgetBuilderServiceProvider());
        $this->app->register(new ImageFactoryServiceProvider());
        $this->app->register(new MailerServiceProvider());
        $this->app->register(new NonceServiceProvider());
        $this->app->register(new SessionServiceProvider());
        $this->app->register(new TwigServiceProvider());
        $this->app->extend(new AssetManagerServiceExtender());
        $this->app->extend(new TwigServiceExtender());
    }

    public function init(): void
    {
        $authenticator = new LoginPageAuthenticator(
            $this->app->get('authentication'),
            $this->app->get('http_factory'),
            $this->app->get('router'),
            $this->app->get('session'),
            'cms'
        );
        $this->app->get('authentication')->createRealm('cms', $this->app->get('cms_user_provider'), $authenticator);
        // Twig.
        $this->app->get('twig')->getLoader()->addPath(__DIR__ . '/../views', 'CmsUi');
    }

    public function install(): void
    {
    }

}
