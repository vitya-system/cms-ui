<?php

/*
 * Copyright 2022, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DatumEditionFormWidget;

use Exception;
use Vitya\CmsComponent\Datum\DatumInterface;
use Vitya\CmsApplication\DatumEditionFormWidget\DatumEditionFormWidgetInterface;
use Vitya\CmsApplication\Widget\AbstractCmsUiEditionFormWidget;
use Vitya\Component\Authentication\UserInterface;

abstract class AbstractDatumEditionFormWidget extends AbstractCmsUiEditionFormWidget implements DatumEditionFormWidgetInterface
{
    private $annotations = [];
    private $baseName = '';
    private $data = [];
    private $options = [];
    private $user = null;

    public function __construct()
    {
        parent::__construct();
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): static
    {
        $this->assertValidData($data);
        $this->data = $data;
        return $this;
    }

    public function getDefaultOptions(): array
    {
        return [];
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): static
    {
        $options = array_merge($this->getDefaultOptions(), $options);
        $this->assertValidOptions($options);
        $this->options = $options;
        return $this;
    }
    
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function updateEntity(): static
    {
        return $this;
    }

    public function getChildren(): array
    {
        return [];
    }

    public function getDescendants(): array
    {
        return [];
    }

    public function addAnnotation(string $annotation): static
    {
        $this->annotations[] = $annotation;
        return $this;
    }

    public function getAnnotations(): array
    {
        return $this->annotations;
    }

    public function assertValidData(array $data): static
    {
        $reference_entity = null;
        $i = 0;
        foreach ($data as $datum) {
            if (false === $datum instanceof DatumInterface) {
                throw new Exception('Data must implement DatumInterface.');
            }
            if (null === $datum->getEntity()) {
                throw new Exception('This datum does not have a parent entity.');
            }
            if ($i > 0) {
                if ($datum->getEntity() !== $reference_entity) {
                    throw new Exception('All data must come from the same entity.');
                }
            }
            $reference_entity = $datum->getEntity();
            $i++;
        }
        return $this;
    }

    public function assertValidOptions(array $options): static
    {
        // Does nothing by default.
        return $this;
    }

}
