<?php

/*
 * Copyright 2022, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DatumEditionFormWidget;

use Vitya\CmsApplication\Widget\CmsUiEditionFormWidgetInterface;
use Vitya\Component\Authentication\UserInterface;

interface DatumEditionFormWidgetInterface extends CmsUiEditionFormWidgetInterface
{
    public function getData(): array;

    public function setData(array $data): static;

    public function getOptions(): array;

    public function setOptions(array $options): static;

    public function getUser(): ?UserInterface;

    public function setUser(UserInterface $user): static;

    public function addAnnotation(string $annotation): static;

    public function getAnnotations(): array;

    public function assertValidData(array $data): static;

    public function assertValidOptions(array $options): static;

}
