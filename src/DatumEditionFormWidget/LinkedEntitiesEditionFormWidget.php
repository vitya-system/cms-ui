<?php

/*
 * Copyright 2022, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DatumEditionFormWidget;

use Exception;
use Twig\Environment;
use Vitya\CmsComponent\Datum\LinkedEntitiesDatum;
use Vitya\CmsApplication\Widget\WidgetValidationResult;
use Vitya\Component\Frontend\WebFrontend;

class LinkedEntitiesEditionFormWidget extends AbstractDatumEditionFormWidget
{
    private $twig;
    private $webFrontend;

    public function __construct(Environment $twig, WebFrontend $web_frontend)
    {
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    public function render(): string
    {
        $twig = $this->twig;
        $options = $this->getOptions();
        $data = $this->getData();
        $datum = $data[0];
        $datum_options = $datum->getOptions();
        if (false === $datum->canBeViewed($this->getUser())) {
            return '';
        }
        $validation_result = $this->getWidgetValidationResult();
        return $twig->render(
            '@CmsUi/DatumWidget/LinkedEntitiesEditionFormWidget/widget.twig',
            [
                'base_name' => $this->getBaseName(),
                'value' => $datum->get(),
                'title' => $options['title'],
                'annotations' => $this->getAnnotations(),
                'can_be_modified' => $datum->canBeModified($this->getUser()),
                'error' => (null !== $validation_result ? $validation_result->isError() : false),
                'html_error_description' => (null !== $validation_result ? $validation_result->getHtmlErrorDescription() : ''),
            ]
        );
    }

    public function updateEntity(): static
    {
        $base_name = $this->getBaseName();
        $main_request = $this->webFrontend->getMainServerRequest();
        $data = $this->getData();
        $options = $this->getOptions();
        $datum = $data[0];
        if (false === $datum->canBeViewed($this->getUser())) {
            return $this;
        }
        if (false === $datum->canBeModified($this->getUser())) {
            return $this;
        }
        $datum_options = $datum->getOptions();
        $params = $main_request->getParsedBody();
        $new_value = [];
        if ((false === isset($params[$base_name . '_class'])) || (false === isset($params[$base_name . '_id']))) {
            return $this;
        }
        if ((false === is_array($params[$base_name . '_class'])) || (false === is_array($params[$base_name . '_id']))) {
            return $this;
        }
        foreach ($params[$base_name . '_class'] as $k => $v) {
            if (isset($params[$base_name . '_id'][$k])) {
                $entity_class = (string) $params[$base_name . '_class'][$k];
                $entity_id = (int) $params[$base_name . '_id'][$k];
                $new_value[] = [
                    'class' => $entity_class,
                    'id' => $entity_id,
                ];
            }
        }
        $datum->set($new_value);
        return $this;
    }

    public function assertValidData(array $data): static
    {
        parent::assertValidData($data);
        if (false === isset($data[0])) {
            throw new Exception('A datum must be provided.');
        }
        if (false === $data[0] instanceof LinkedEntitiesDatum) {
            throw new Exception('Invalid datum type.');
        }
        return $this;
    }

    public function assertValidOptions(array $options): static
    {
        parent::assertValidOptions($options);
        if (false === is_string($options['title'])) {
            throw new Exception('The "title" option must be defined as a string.');
        }
        return $this;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getWebFrontend(): WebFrontend
    {
        return $this->webFrontend;
    }

    public function updateValidationResult(): static
    {
        $data = $this->getData();
        $datum = $data[0];
        $widget_validation_result = new WidgetValidationResult();
        $label = $this->getOptions()['title'];
        foreach ($this->getAnnotations() as $annotation) {
            $label .= ' · ' . $annotation;
        }
        if (false === $datum->isInAValidState()) {
            $widget_validation_result->setError(true);
            $validation_errors = $datum->getValidationErrors();
        }
        $this->setWidgetValidationResult($widget_validation_result);
        return $this;
    }

}
