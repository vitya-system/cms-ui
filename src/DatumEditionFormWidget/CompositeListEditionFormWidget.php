<?php

/*
 * Copyright 2024, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DatumEditionFormWidget;

use Exception;
use Twig\Environment;
use Vitya\CmsComponent\Composite\CompositeFactoryInterface;
use Vitya\CmsComponent\Datum\CompositeListDatum;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\CompositeCmsAdminIntegration\HasACompositeCmsAdminIntegrationInterface;
use Vitya\CmsApplication\Widget\WidgetValidationResult;
use Vitya\Component\Frontend\WebFrontend;
use Vitya\Component\Service\DependencyInjectorInterface;

class CompositeListEditionFormWidget extends AbstractDatumEditionFormWidget
{
    private $cmsUi = null;
    private $compositeFactory = null;
    private $dependencyInjector = null;
    private $twig = null;
    private $webFrontend = null;
    private $validationNeeded = false;

    public function __construct(
        CmsUi $cms_ui,
        CompositeFactoryInterface $composite_factory,
        DependencyInjectorInterface $dependency_injector,
        Environment $twig,
        WebFrontend $web_frontend
    )
    {
        $this->cmsUi = $cms_ui;
        $this->compositeFactory = $composite_factory;
        $this->dependencyInjector = $dependency_injector;
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    public function getCmsUi(): CmsUi
    {
        return $this->cmsUi;
    }

    public function getCompositeFactory(): CompositeFactoryInterface
    {
        return $this->compositeFactory;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getWebFrontend(): WebFrontend
    {
        return $this->webFrontend;
    }

    public function validationIsNeeded(): bool
    {
        return $this->validationNeeded;
    }

    public function setValidationNeeded(bool $validation_needed): static
    {
        $this->validationNeeded = $validation_needed;
        return $this;
    }

    public function render(): string
    {
        $twig = $this->twig;
        $options = $this->getOptions();
        $data = $this->getData();
        $datum = $data[0];
        $datum_options = $datum->getOptions();
        if (false === $datum->canBeViewed($this->getUser())) {
            return '';
        }
        $validation_result = $this->getWidgetValidationResult();
        $blocks_description = [];
        foreach ($datum->getChildren() as $k => $composite) {
            if (false === $composite instanceof HasACompositeCmsAdminIntegrationInterface) {
                continue;
            }
            $composite_cms_admin_integration = $this->getDependencyInjector()->make($composite->getCompositeCmsAdminIntegrationClassName());
            $composite_cms_admin_integration->setComposite($composite);
            $composite_widget = $composite_cms_admin_integration->makeEditionFormWidget();
            if ($this->validationIsNeeded()) {
                $composite_widget->updateValidationResult();
            }
            $blocks_description[$k] = [
                'name' => $composite_cms_admin_integration->getName(),
                'composite_class' => (string) get_class($composite),
                'widget' => $composite_widget,
            ];
        }
        $supported_classes = $datum_options['supported_classes'];
        $add_block_options = [];
        foreach ($supported_classes as $supported_class) {
            $supported_composite = $this->getCompositeFactory()->makeComposite($supported_class);
            if (null === $supported_composite) {
                continue;
            }
            if (false === $supported_composite instanceof HasACompositeCmsAdminIntegrationInterface) {
                continue;
            }
            $supported_composite_cms_admin_integration = $this->getDependencyInjector()->make($supported_composite->getCompositeCmsAdminIntegrationClassName());
            $supported_composite_cms_admin_integration->setComposite($supported_composite);
            $add_block_options[$supported_class] = [
                'supported_class' => $supported_class,
                'label' => $supported_composite_cms_admin_integration->getName(),
            ];
        }
        return $twig->render(
            '@CmsUi/DatumWidget/CompositeListEditionFormWidget/widget.twig',
            [
                'base_name' => $this->getBaseName(),
                'blocks_description' => $blocks_description,
                'title' => $options['title'],
                'annotations' => $this->getAnnotations(),
                'can_be_modified' => $datum->canBeModified($this->getUser()),
                'error' => (null !== $validation_result ? $validation_result->isError() : false),
                'html_error_description' => (null !== $validation_result ? $validation_result->getHtmlErrorDescription() : ''),
                'entity' => $datum->getEntity(),
                'add_block_options' => $add_block_options,
            ]
        );
    }

    public function updateEntity(): static
    {
        $base_name = $this->getBaseName();
        $main_request = $this->webFrontend->getMainServerRequest();
        $data = $this->getData();
        $options = $this->getOptions();
        $datum = $data[0];
        if (false === $datum->canBeViewed($this->getUser())) {
            return $this;
        }
        if (false === $datum->canBeModified($this->getUser())) {
            return $this;
        }
        $datum_options = $datum->getOptions();
        $params = $main_request->getParsedBody();
        $composite_ids = [];
        if (isset($params[$base_name]) && is_array($params[$base_name])) {
            $composite_ids = $params[$base_name];
        }
        $composite_classes = [];
        if (isset($params[$base_name . '-class']) && is_array($params[$base_name . '-class'])) {
            $composite_classes = $params[$base_name . '-class'];
        }
        $datum->set([]);
        foreach ($composite_ids as $k => $composite_id) {
            $composite_id = (string) $composite_id;
            if (false === isset($composite_classes[$k])) {
                continue;
            }
            $composite_class = (string) $composite_classes[$k];
            $composite = $this->getCompositeFactory()->makeComposite($composite_class);
            if (false === $composite instanceof HasACompositeCmsAdminIntegrationInterface) {
                continue;
            }
            $composite_cms_admin_integration = $this->getDependencyInjector()->make($composite->getCompositeCmsAdminIntegrationClassName());
            $composite_cms_admin_integration->setComposite($composite);
            $composite->setPartOfAStaticHierarchy(false);
            foreach ($composite->getDescendants() as $descendant) {
                $descendant->setPartOfAStaticHierarchy(false);
            }
            $datum->addChild((string) $composite_id, $composite);
            $composite_widget = $composite_cms_admin_integration->makeEditionFormWidget();
            $composite_widget->updateEntity();
        }
        return $this;
    }

    public function assertValidData(array $data): static
    {
        parent::assertValidData($data);
        if (false === isset($data[0])) {
            throw new Exception('A datum must be provided.');
        }
        if (false === $data[0] instanceof CompositeListDatum) {
            throw new Exception('Invalid datum type.');
        }
        return $this;
    }

    public function assertValidOptions(array $options): static
    {
        parent::assertValidOptions($options);
        if (false === is_string($options['title'])) {
            throw new Exception('The "title" option must be defined as a string.');
        }
        return $this;
    }

    public function updateValidationResult(): static
    {
        $data = $this->getData();
        $datum = $data[0];
        $widget_validation_result = new WidgetValidationResult();
        $label = $this->getOptions()['title'];
        foreach ($this->getAnnotations() as $annotation) {
            $label .= ' · ' . $annotation;
        }
        $contains_errors = false;
        foreach ($datum->getDescendants() as $descendant) {
            if (false === $descendant->isInAValidState()) {
                $contains_errors = true;
            }
        }
        if ($contains_errors) {
            $widget_validation_result->setError(true);
            $widget_validation_result->addHtmlErrorMessage('Check for errors in <em>' . htmlspecialchars($label) . '</em>.');
        }
        $this->setWidgetValidationResult($widget_validation_result);
        $this->setValidationNeeded(true);
        return $this;
    }

}
