<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\CompositeCmsAdminIntegration;

use Exception;
use Vitya\CmsComponent\Composite\CompositeInterface;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Widget\CompositeEditionFormWidget;
use Vitya\CmsApplication\Widget\EntityWidgetBuilder;
use Vitya\Component\Service\DependencyInjectorInterface;

abstract class AbstractCompositeCmsAdminIntegration implements CompositeCmsAdminIntegrationInterface
{
    private $cmsUi = null;
    private $composite = null;
    private $dependencyInjector = null;
    private $entityWidgetBuilder = null;

    public function __construct(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        EntityWidgetBuilder $entity_widget_builder
    )
    {
        $this->cmsUi = $cms_ui;
        $this->dependencyInjector = $dependency_injector;
        $this->entityWidgetBuilder = $entity_widget_builder;
    }

    public function getCmsUi(): CmsUi
    {
        return $this->cmsUi;
    }

    public function getComposite(): CompositeInterface
    {
        return $this->composite;
    }

    public function setComposite(CompositeInterface $composite): static
    {
        if (false === $composite instanceof ($this->getSupportedCompositeClass())) {
            throw new Exception(''
                . 'This type of composite is not supported by this CmsAdminIntegration. '
                . 'Expected: ' . $this->getSupportedCompositeClass() . '. '
                . 'Given: ' . get_class($composite) . '.');
        }
        $this->composite = $composite;
        return $this;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getEntityWidgetBuilder(): EntityWidgetBuilder
    {
        return $this->entityWidgetBuilder;
    }

    public function getName(): string
    {
        return 'composite';
    }

    public function makeEditionFormWidget(): CompositeEditionFormWidget
    {
        $widget = $this->getDependencyInjector()->make('Vitya\CmsApplication\Widget\CompositeEditionFormWidget');
        $composite = $this->getComposite();
        $block_widgets = $this->getEntityWidgetBuilder()->createWidgetsFromDescription(
            $this->getEditionFormBlockDescription(),
            $composite,
            $this->getCmsUi()->getAuthenticationService()->getUser('cms'),
            null // We don't set an origin URI.
        );
        $block_widget_list = [];
        foreach ($block_widgets as $block_widget) {
            $block_widget_list[] = $block_widget;
            foreach ($block_widget->getDescendants() as $descendant_widget) {
                $block_widget_list[] = $descendant_widget;
            }
        }
        $counter = 0;
        foreach ($block_widget_list as $block_widget) {
            $block_widget->setBaseName($this->getEntityWidgetBuilder()->generateBaseName($composite->getEntity()->getAddress() .'+' . $composite->getId(), $counter++));
        }
        $widget->setBlockWidgets($block_widgets);
        return $widget;
    }

    public function getEditionFormBlockDescription(): array
    {
        return [];
    }

    abstract public function getSupportedCompositeClass(): string;

}
