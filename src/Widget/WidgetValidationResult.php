<?php

/*
 * Copyright 2023, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

class WidgetValidationResult implements WidgetValidationResultInterface
{
    private $error = false;
    private $htmlErrorMessages = [];
    private $htmlErrorDescription = '';

    public function isError(): bool
    {
        return $this->error;
    }

    public function setError(bool $error): self
    {
        $this->error = $error;
        return $this;
    }

    public function getHtmlErrorMessages(): array
    {
        return $this->htmlErrorMessages;
    }

    public function addHtmlErrorMessage(string $html_error_message): self
    {
        $this->htmlErrorMessages[] = $html_error_message;
        return $this;
    }

    public function getHtmlErrorDescription(): string
    {
        return $this->htmlErrorDescription;
    }

    public function setHtmlErrorDescription(string $html_error_description): self
    {
        $this->htmlErrorDescription = $html_error_description;
        return $this;
    }

}
