<?php

/*
 * Copyright 2023, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Twig\Environment;
use Vitya\CmsComponent\EntityComponent\ImageEntityComponent;
use Vitya\CmsApplication\CmsUi;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Frontend\WebFrontend;

class ImageEntityComponentWidget extends AbstractCmsUiEditionFormWidget
{
    private $entityComponent = null;
    private $cmsUi = null;
    private $twig = null;
    private $user = null;
    private $webFrontend = null;

    public function __construct(CmsUi $cms_ui, Environment $twig, WebFrontend $web_frontend)
    {
        parent::__construct();
        $this->cmsUi = $cms_ui;
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    public function render(): string
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('An entity component must be set before rendering the widget.');
        }
        if (false === $entity_component->canBeViewed($this->user)) {
            return '';
        }
        $entity = $entity_component->getEntity();
        return $this->twig->render(
            '@CmsUi/Widget/ImageEntityComponentWidget/widget.twig',
            [
                'base_name' => $this->getBaseName(),
                'entity_type' => $entity->getMachineName(),
                'entity_id' => $entity->getId(),
                'entity_component' => $entity_component->get(),
                'can_be_modified' => $entity_component->canBeModified($this->user),
                'formatted_size' => $entity_component->getFormattedSize(),
                'component_address' => $entity_component->getAddress(),
            ]
        );
    }

    public function updateEntity(): static
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('No entity component set.');
        }
        if (false === $entity_component->canBeModified($this->user)) {
            return $this;
        }
        $base_name = $this->getBaseName();
        $main_request = $this->webFrontend->getMainServerRequest();
        $params = $main_request->getParsedBody();
        $uploaded_files = $main_request->getUploadedFiles();
        if (isset($uploaded_files[$base_name]) && \UPLOAD_ERR_OK === $uploaded_files[$base_name]->getError()) {
            $this->getEntityComponent()->setUploadedFile($uploaded_files[$base_name]);
        } else {
            $content = $this->getEntityComponent()->get();
            if (isset($params[$base_name . '_autocrop'])) {
                $content['autocrop'] = true;
            } else {
                $content['autocrop'] = false;
            }
            if (isset($params[$base_name . '_coi_x'])) {
                $content['coi_x'] = (float) $params[$base_name . '_coi_x'];
            }
            if (isset($params[$base_name . '_coi_y'])) {
                $content['coi_y'] = (float) $params[$base_name . '_coi_y'];
            }
            $this->getEntityComponent()->set($content);
        }
        return $this;
    }

    public function getCmsUi(): CmsUi
    {
        return $this->cmsUi;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getWebFrontend(): WebFrontend
    {
        return $this->webFrontend;
    }

    public function setEntityComponent(ImageEntityComponent $entity_component): static
    {
        $this->entityComponent = $entity_component;
        return $this;
    }

    public function getEntityComponent(): ?ImageEntityComponent
    {
        return $this->entityComponent;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

}
