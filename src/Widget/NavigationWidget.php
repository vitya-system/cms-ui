<?php

/*
 * Copyright 2021, 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsComponent\Entity\EntityFactoryInterface;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Route\RouterInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class NavigationWidget implements CmsUiPageWidgetInterface
{
    private $authn = null;
    private $cmsUi = null;
    private $dependencyInjector = null;
    private $entityFactory = null;
    private $router = null;
    private $twig = null;
    private $navigation = [];

    public function __construct(
        AuthenticationService $authn,
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        EntityFactoryInterface $entity_factory,
        RouterInterface $router,
        Environment $twig
    ) {
        $this->authn = $authn;
        $this->cmsUi = $cms_ui;
        $this->dependencyInjector = $dependency_injector;
        $this->entityFactory = $entity_factory;
        $this->router = $router;
        $this->twig = $twig;
        $navigation_group = [
            [
                'label' => 'Dashboard',
                'url' => (string) $this->cmsUi->decoratePageUri($this->router->createUri('cms-ui-dashboard')),
            ],
        ];
        $this->navigation[] = $navigation_group;
        $navigation_group = [];
        foreach ($this->entityFactory->getAvailableEntityClassNames() as $entity_class_name) {
            $model = $this->entityFactory->make($entity_class_name);
            if (false === $model instanceof HasAnEntityCmsAdminIntegrationInterface) {
                continue;
            }
            $cms_admin_integration = $this->dependencyInjector->make($model->getEntityCmsAdminIntegrationClassName());
            $cms_admin_integration->setEntity($model);
            $entry_point_uri = $cms_admin_integration->getEntryPointUri();
            if (null !== $entry_point_uri) {
                $navigation_group[] = [
                    'label' => ucfirst($cms_admin_integration->getCollectionName()),
                    'url' => (string) $this->cmsUi->decoratePageUri($entry_point_uri),
                ];
            }
        }
        $this->navigation[] = $navigation_group;
        if ($cms_ui->userIsSysadmin()) {
            $navigation_group = [
                [
                    'label' => 'Logs',
                    'url' => (string) $this->cmsUi->decoratePageUri($this->router->createUri('cms-ui-system-logs')),
                ],
            ];
            $this->navigation[] = $navigation_group;
        }
    }

    public function render(): string
    {
        return $this->twig->render(
            '@CmsUi/Widget/NavigationWidget/navigation-widget.twig',
            [
                'navigation' => $this->navigation,
            ]
        );
    }

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface
    {
        return $uri;
    }

}
