<?php

/*
 * Copyright 2023, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Twig\Environment;
use Vitya\CmsComponent\EntityComponent\EmbedEntityComponent;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Frontend\WebFrontend;

class EmbedEntityComponentWidget extends AbstractCmsUiEditionFormWidget
{
    private $entityComponent = null;
    private $twig = null;
    private $webFrontend = null;
    private $user = null;

    public function __construct(Environment $twig, WebFrontend $web_frontend)
    {
        parent::__construct();
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    public function render(): string
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('An entity component must be set before rendering the widget.');
        }
        if (false === $entity_component->canBeViewed($this->user)) {
            return '';
        }
        $entity = $entity_component->getEntity();
        return $this->twig->render(
            '@CmsUi/Widget/EmbedEntityComponentWidget/widget.twig',
            [
                'base_name' => $this->getBaseName(),
                'entity_type' => $entity->getMachineName(),
                'entity_id' => $entity->getId(),
                'entity_component' => $entity_component->get(),
                'can_be_modified' => $entity_component->canBeModified($this->user),
                'component_address' => $entity_component->getAddress(),
            ]
        );
    }

    public function updateEntity(): static
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('No entity component set.');
        }
        if (false === $entity_component->canBeModified($this->user)) {
            return $this;
        }
        $base_name = $this->getBaseName();
        $main_request = $this->webFrontend->getMainServerRequest();
        $params = $main_request->getParsedBody();
        $content = [
            'source_url' => (isset($params[$base_name . '_source_url']) ? (string) $params[$base_name . '_source_url'] : ''),
            'title' => (isset($params[$base_name . '_title']) ? (string) $params[$base_name . '_title'] : ''),
            'description' => (isset($params[$base_name . '_description']) ? (string) $params[$base_name . '_description'] : ''),
            'url' => (isset($params[$base_name . '_url']) ? (string) $params[$base_name . '_url'] : ''),
            'keywords' => (isset($params[$base_name . '_keywords']) ? (string) $params[$base_name . '_keywords'] : ''),
            'image' => (isset($params[$base_name . '_image']) ? (string) $params[$base_name . '_image'] : ''),
            'html' => (isset($params[$base_name . '_html']) ? (string) $params[$base_name . '_html'] : ''),
            'width' => (isset($params[$base_name . '_width']) ? (int) $params[$base_name . '_width'] : 0),
            'height' => (isset($params[$base_name . '_height']) ? (int) $params[$base_name . '_height'] : 0),
            'ratio' => (isset($params[$base_name . '_ratio']) ? (float) $params[$base_name . '_ratio'] : 0.0),
            'author_name' => (isset($params[$base_name . '_author_name']) ? (string) $params[$base_name . '_author_name'] : ''),
            'author_url' => (isset($params[$base_name . '_author_url']) ? (string) $params[$base_name . '_author_url'] : ''),
            'cms' => (isset($params[$base_name . '_cms']) ? (string) $params[$base_name . '_cms'] : ''),
            'language' => (isset($params[$base_name . '_language']) ? (string) $params[$base_name . '_language'] : ''),
            'languages' => (isset($params[$base_name . '_languages']) ? (string) $params[$base_name . '_languages'] : ''),
            'provider_name' => (isset($params[$base_name . '_provider_name']) ? (string) $params[$base_name . '_provider_name'] : ''),
            'provider_url' => (isset($params[$base_name . '_provider_url']) ? (string) $params[$base_name . '_provider_url'] : ''),
            'icon' => (isset($params[$base_name . '_icon']) ? (string) $params[$base_name . '_icon'] : ''),
            'favicon' => (isset($params[$base_name . '_favicon']) ? (string) $params[$base_name . '_favicon'] : ''),
            'published_time' => (isset($params[$base_name . '_published_time']) ? (string) $params[$base_name . '_published_time'] : ''),
            'license' => (isset($params[$base_name . '_license']) ? (string) $params[$base_name . '_license'] : ''),
            'feeds' => (isset($params[$base_name . '_feeds']) ? (string) $params[$base_name . '_feeds'] : ''),
        ];
        $entity_component->set($content);
        return $this;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getWebFrontend(): WebFrontend
    {
        return $this->webFrontend;
    }

    public function setEntityComponent(EmbedEntityComponent $entity_component): static
    {
        $this->entityComponent = $entity_component;
        return $this;
    }

    public function getEntityComponent(): ?EmbedEntityComponent
    {
        return $this->entityComponent;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

}
