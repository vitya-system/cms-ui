<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\EntityCmsAdminIntegration\EntityCmsAdminIntegrationInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Widget\WidgetInterface;

class EntityCardWidget implements WidgetInterface
{
    private $cmsUi = null;
    private $draggable = true;
    private $entityCmsAdminIntegration = null;
    private $localeCatalog = null;
    private $originUri = null;
    private $twig = null;
    private $withLinks = false;

    public function __construct(CmsUi $cms_ui, LocaleCatalog $locale_catalog, Environment $twig)
    {
        $this->cmsUi = $cms_ui;
        $this->localeCatalog = $locale_catalog;
        $this->twig = $twig;
    }

    public function render(): string
    {
        $entity = $this->getEntityCmsAdminIntegration()->getEntity();
        $edition_form_uri = $this->getEntityCmsAdminIntegration()->getEditionFormUri($this->originUri);
        $edition_form_uri = $this->cmsUi->decoratePageUri($edition_form_uri);
        $name = $this->getEntityCmsAdminIntegration()->getName();
        $locale = $this->getCmsUi()->getWorkingLocale()->getId();
        if (null === $locale || false === $this->getLocaleCatalog()->hasLocale($locale)) {
            $locale = $this->getLocaleCatalog()->getDefaultLocale()->getId();
        }
        return $this->twig->render(
            '@CmsUi/Widget/EntityCardWidget/card.twig',
            [
                'name' => $name,
                'entity' => $entity,
                'title' => $this->getEntityCmsAdminIntegration()->getTitle($locale),
                'edition_form_uri' => $edition_form_uri,
                'with_links' => $this->withLinks,
                'draggable' => $this->draggable,
            ]
        );
    }

    public function getCmsUi(): CmsUi
    {
        return $this->cmsUi;
    }

    public function isDraggable(): bool
    {
        return $this->draggable;
    }

    public function setDraggable(bool $draggable): static
    {
        $this->draggable = $draggable;
        return $this;
    }

    public function getEntityCmsAdminIntegration(): EntityCmsAdminIntegrationInterface
    {
        if (null === $this->entityCmsAdminIntegration) {
            throw new Exception('No EntityCmsAdminIntegration was set.');
        }
        return $this->entityCmsAdminIntegration;
    }

    public function setEntityCmsAdminIntegration(EntityCmsAdminIntegrationInterface $entity_cms_admin_integration): static
    {
        $this->entityCmsAdminIntegration = $entity_cms_admin_integration;
        return $this;
    }

    public function getLocaleCatalog(): LocaleCatalog
    {
        return $this->localeCatalog;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getOriginUri(): ?UriInterface
    {
        return $this->originUri;
    }

    public function setOriginUri(UriInterface $origin_uri = null): static
    {
        $this->originUri = $origin_uri;
        return $this;
    }

    public function isWithLinks(): bool
    {
        return $this->withLinks;
    }

    public function setWithLinks(bool $with_links): static
    {
        $this->withLinks = $with_links;
        return $this;
    }

}
