<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\Application\Helper\PaginationHelper;
use Vitya\Component\Frontend\WebFrontend;

class PaginationWidget implements CmsUiPageWidgetInterface
{
    private $twig = null;
    private $baseIndexUri = null;
    private $decoratedIndexUri = null;
    private $webFrontend = '';
    private $paginationHelper = null;

    public function __construct(
        Environment $twig,
        UriInterface $base_index_uri,
        UriInterface $decorated_index_uri,
        WebFrontend $web_frontend,
        int $nb_items,
        array $possible_nbs_items_per_page
    ) {
        $this->twig = $twig;
        $this->baseIndexUri = $base_index_uri;
        $this->decoratedIndexUri = $decorated_index_uri;
        $this->webFrontend = $web_frontend;
        $this->paginationHelper = new PaginationHelper($this->webFrontend->getMainServerRequest());
        $this->paginationHelper->setNbItems($nb_items);
        $this->paginationHelper->setPossibleNbsItemsPerPage($possible_nbs_items_per_page);
    }

    public function getPaginationHelper(): PaginationHelper
    {
        return $this->paginationHelper;
    }

    public function render(): string
    {
        $first_page = null;
        if (1 !== $this->paginationHelper->getCurrentPage()) {
            $first_page = [
                'url' => (string) $this->paginationHelper->getDecoratedUri($this->decoratedIndexUri, 1, $this->paginationHelper->getNbItemsPerPage()),
            ];
        }
        $previous_page = null;
        if (1 < $this->paginationHelper->getCurrentPage()) {
            $previous_page = [
                'url' => (string) $this->paginationHelper->getDecoratedUri($this->decoratedIndexUri, $this->paginationHelper->getCurrentPage() - 1, $this->paginationHelper->getNbItemsPerPage()),
            ];
        }
        $pages = [];
        $first_displayed_page_nb = 1;
        $last_displayed_page_nb = 1;
        if (5 >= $this->paginationHelper->getNbPages()) {
            // Five pages max; we can display all of them.
            $first_displayed_page_nb = 1;
            $last_displayed_page_nb = $this->paginationHelper->getNbPages();
        } else {
            if (3 >= $this->paginationHelper->getCurrentPage()) {
                // We're in the first three pages.
                $first_displayed_page_nb = 1;
                $last_displayed_page_nb = 5;
            } elseif (3 > $this->paginationHelper->getNbPages() - $this->paginationHelper->getCurrentPage()) {
                // We're in the last three pages.
                $first_displayed_page_nb = $this->paginationHelper->getNbPages() - 4;
                $last_displayed_page_nb = $this->paginationHelper->getNbPages();
            } else {
                // We're in the middle of the list.
                $first_displayed_page_nb = $this->paginationHelper->getCurrentPage() - 2;
                $last_displayed_page_nb = $this->paginationHelper->getCurrentPage() + 2;
            }
        }
        for ($i = $first_displayed_page_nb; $i <= $last_displayed_page_nb; $i++) {
            $pages[$i] = [
                'url' => (string) $this->paginationHelper->getDecoratedUri($this->decoratedIndexUri, $i, $this->paginationHelper->getNbItemsPerPage()),
                'current' => ($this->paginationHelper->getCurrentPage() === $i),
            ];
        }
        $next_page = null;
        if ($this->paginationHelper->getNbPages() > $this->paginationHelper->getCurrentPage()) {
            $next_page = [
                'url' => (string) $this->paginationHelper->getDecoratedUri($this->decoratedIndexUri, $this->paginationHelper->getCurrentPage() + 1, $this->paginationHelper->getNbItemsPerPage()),
            ];
        }
        $last_page = null;
        if ($this->paginationHelper->getNbPages() > $this->paginationHelper->getCurrentPage()) {
            $last_page = [
                'url' => (string) $this->paginationHelper->getDecoratedUri($this->decoratedIndexUri, $this->paginationHelper->getNbPages(), $this->paginationHelper->getNbItemsPerPage()),
            ];
        }
        $possible_nbs_items_per_page = [];
        foreach ($this->paginationHelper->getPossibleNbsItemsPerPage() as $nb) {
            $possible_nbs_items_per_page[$nb] = [
                'url' => (string) $this->paginationHelper->getDecoratedUri($this->decoratedIndexUri, 1, $nb),
                'current' => $this->paginationHelper->getNbItemsPerPage() === $nb,
            ];
        }
        return $this->twig->render(
            '@CmsUi/Widget/PaginationWidget/pagination-widget.twig',
            [
                'nb_pages' => $this->paginationHelper->getNbPages(),
                'nb_items' => $this->paginationHelper->getNbItems(),
                'first_item_offset' => $this->paginationHelper->getFirstItemOffset(),
                'last_item_offset' => $this->paginationHelper->getLastItemOffset(),
                'first_page' => $first_page,
                'previous_page' => $previous_page,
                'pages' => $pages,
                'next_page' => $next_page,
                'last_page' => $last_page,
                'possible_nbs_items_per_page' => $possible_nbs_items_per_page,
            ]
        );
    }

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface
    {
        if (null === $uri) {
            return null;
        }
        //$uri = $this->paginationHelper->getDecoratedUri($uri, )
        return $uri;
    }

}
