<?php

/*
 * Copyright 2023, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsComponent\EntityComponent\UserManagementEntityComponent;
use Vitya\CmsApplication\CmsUi;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Frontend\WebFrontend;

class UserManagementEntityComponentWidget extends AbstractCmsUiEditionFormWidget
{
    private $entityComponent = null;
    private $cmsUi = null;
    private $twig = null;
    private $webFrontend = null;
    private $user = null;
    private $originUri = null;

    public function __construct(CmsUi $cms_ui, Environment $twig, WebFrontend $web_frontend)
    {
        parent::__construct();
        $this->cmsUi = $cms_ui;
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    public function render(): string
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('An entity component must be set before rendering the widget.');
        }
        if (false === $entity_component->canBeViewed($this->user)) {
            return '';
        }
        return $this->twig->render(
            '@CmsUi/Widget/UserManagementEntityComponentWidget/widget.twig',
            [
                'base_name' => $this->getBaseName(),
                'omnipotent' => $entity_component->isOmnipotent(),
                'groups' => $entity_component->getGroups(),
                'user_groups' => $entity_component->getAuthenticationService()->getUserProvider($entity_component->getAuthenticationRealm())->getUserGroups(),
                'can_be_modified' => $entity_component->canBeModified($this->user),
                'can_be_made_omnipotent' => $entity_component->canBeMadeOmnipotent($this->user),
            ]
        );
    }

    public function updateEntity(): static
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('No entity component set.');
        }
        if (false === $entity_component->canBeModified($this->user)) {
            return $this;
        }
        $main_request = $this->webFrontend->getMainServerRequest();
        $params = $main_request->getParsedBody();
        $new_groups = [];
        $available_user_groups = $entity_component->getAuthenticationService()->getUserProvider($entity_component->getAuthenticationRealm())->getUserGroups();
        if (isset($params[$this->getBaseName() . '_user_groups']) && is_array($params[$this->getBaseName() . '_user_groups'])) {
            foreach ($params[$this->getBaseName() . '_user_groups'] as $group_identifier) {
                $group_identifier = (int) $group_identifier;
                if (isset($available_user_groups[$group_identifier])) {
                    $new_groups[] = $group_identifier;
                }
            }
            $entity_component->setGroups($new_groups);
        }
        if ($entity_component->canBeMadeOmnipotent($this->user)) {
            $omnipotent = false;
            if (isset($params[$this->getBaseName() . '_omnipotent']) && '1' === $params[$this->getBaseName() . '_omnipotent']) {
                $omnipotent = true;
            }
            $entity_component->setOmnipotent($omnipotent);
        }
        return $this;
    }

    public function getCmsUi(): CmsUi
    {
        return $this->cmsUi;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getWebFrontend(): WebFrontend
    {
        return $this->webFrontend;
    }

    public function setEntityComponent(UserManagementEntityComponent $entity_component): static
    {
        $this->entityComponent = $entity_component;
        return $this;
    }

    public function getEntityComponent(): ?UserManagementEntityComponent
    {
        return $this->entityComponent;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function getOriginUri(): ?UriInterface
    {
        return $this->originUri;
    }

    public function setOriginUri(UriInterface $origin_uri): UserManagementEntityComponentWidget
    {
        $this->originUri = $origin_uri;
        return $this;
    }

}
