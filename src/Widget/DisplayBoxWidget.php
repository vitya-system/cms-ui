<?php

/*
 * Copyright 2022, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsComponent\EntityListModifier\EntitySort;
use Vitya\CmsApplication\DisplayBoxFilterWidget\DisplayBoxFilterWidgetInterface;
use Vitya\CmsApplication\DisplayBoxSortOption\DisplayBoxSortOptionInterface;
use Vitya\Component\Frontend\WebFrontend;

class DisplayBoxWidget implements CmsUiPageWidgetInterface
{
    private $twig = null;
    private $baseIndexUri = null;
    private $decoratedIndexUri = null;
    private $webFrontend = '';
    private $sortOptions = [];
    private $selectedSortOption = '';
    private $filterWidgets = [];

    public function __construct(Environment $twig, UriInterface $base_index_uri, UriInterface $decorated_index_uri, WebFrontend $web_frontend) {
        $this->twig = $twig;
        $this->baseIndexUri = $base_index_uri;
        $this->decoratedIndexUri = $decorated_index_uri;
        $this->webFrontend = $web_frontend;
        $this->selectedSortOption = '';
        $query_string_parameters = $this->webFrontend->getMainServerRequest()->getQueryParams();
        if (isset($query_string_parameters['sort']) && is_string($query_string_parameters['sort'])) {
            $this->selectedSortOption = $query_string_parameters['sort'];
        }
    }

    public function render(): string
    {
        // Note: we support only one level of query string parameters nesting.
        parse_str($this->decoratedIndexUri->getQuery(), $query_string_parameters);
        $flattened_query_string_parameters = array();
        foreach ($query_string_parameters as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $kk => $vv) {
                    $flattened_query_string_parameters[$k . '[' . $kk . ']'] = (string) $vv;
                }
            } else {
                $flattened_query_string_parameters[$k] = (string) $v;
            }
        }
        $sort_options_array = [];
        foreach ($this->sortOptions as $sort_option) {
            $group = [
                'id' => $sort_option->getId(),
                'label' => $sort_option->getLabel(),
                'asc_label' => $sort_option->getAscLabel(),
                'desc_label' => $sort_option->getDescLabel(),
                'default_direction' => $sort_option->getDefaultDirection(),
            ];
            $sort_options_array[] = $group;
        }
        return $this->twig->render(
            '@CmsUi/Widget/DisplayBoxWidget/widget.twig',
            [
                'base_index_uri' => $this->baseIndexUri,
                'current_query_string_parameters' => $flattened_query_string_parameters,
                'sort_options' => $sort_options_array,
                'selected_sort_option' => $this->getSelectedSortOption(),
                'filter_widgets' => $this->filterWidgets,
            ]
        );
    }

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface
    {
        if (null === $uri) {
            return null;
        }
        if ($this->getSelectedSortOption() !== $this->getDefaultSortOption()) {
            parse_str($uri->getQuery(), $query_params);
            $query_params['sort'] = $this->getSelectedSortOption();
            $uri = $uri->withQuery(http_build_query($query_params));
        }
        foreach ($this->filterWidgets as $filter_widget) {
            $uri = $filter_widget->decoratePageUri($uri);
        }
        return $uri;
    }

    public function getSortOptions(): array
    {
        return $this->sortOptions;
    }

    public function addSortOption(DisplayBoxSortOptionInterface $sort_option): DisplayBoxWidget
    {
        $this->sortOptions[] = $sort_option;
        return $this;
    }

    public function addSortOptions(array $sort_options): DisplayBoxWidget
    {
        foreach ($sort_options as $sort_option) {
            if (false === $sort_option instanceof DisplayBoxSortOptionInterface) {
                throw new Exception('Display box sort option must implement DisplayBoxSortOptionInterface.');
            }
            $this->addSortOption($sort_option);
        }
        return $this;
    }

    public function getSelectedSortOption(): string
    {
        if ('' === $this->selectedSortOption) {
            return $this->getDefaultSortOption();
        }
        return $this->selectedSortOption;
    }

    public function getDefaultSortOption(): string
    {
        if (isset($this->sortOptions[0])) {
            return $this->sortOptions[0]->getId() . '_' . strtolower($this->sortOptions[0]->getDefaultDirection());
        }
        return '';
    }

    public function getEntityListModifiers(): array
    {
        $entity_list_modifiers = [];
        $main_direction = 'DESC';
        foreach ($this->sortOptions as $sort_option) {
            if ($sort_option->getId() . '_asc' === $this->getSelectedSortOption()) {
                $entity_list_modifiers = $sort_option->getEntityListModifiers('ASC');
                $main_direction = 'ASC';
            } elseif ($sort_option->getId() . '_desc' === $this->getSelectedSortOption()) {
                $entity_list_modifiers = $sort_option->getEntityListModifiers('DESC');
                $main_direction = 'DESC';
            }
        }
        // Add a secondary sort option so that we never end up with an ambiguous result.
        $entity_list_modifiers[] = new EntitySort('id', $main_direction);
        foreach ($this->filterWidgets as $filter_widget) {
            $modifiers = $filter_widget->getEntityListModifiers();
            foreach ($modifiers as $modifier) {
                $entity_list_modifiers[] = $modifier;
            }
        }
        return $entity_list_modifiers;
    }

    public function addFilterWidget(DisplayBoxFilterWidgetInterface $filter_widget): DisplayBoxWidget
    {
        $this->filterWidgets[] = $filter_widget;
        return $this;
    }

    public function addFilterWidgets(array $filter_widgets): DisplayBoxWidget
    {
        foreach ($filter_widgets as $filter_widget) {
            if (false === $filter_widget instanceof DisplayBoxFilterWidgetInterface) {
                throw new Exception('Display box filter widget must implement DisplayBoxFilterWidgetInterface.');
            }
            $this->addFilterWidget($filter_widget);
        }
        return $this;
    }

}
