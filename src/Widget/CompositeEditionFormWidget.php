<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Twig\Environment;

class CompositeEditionFormWidget extends AbstractCmsUiEditionFormWidget
{
    private $blockWidgets = [];
    private $twig = null;

    public function __construct(Environment $twig)
    {
        parent::__construct();
        $this->twig = $twig;
    }

    public function getBlockWidgets(): array
    {
        return $this->blockWidgets;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function setBlockWidgets(array $widgets): static
    {
        $this->blockWidgets = [];
        foreach ($widgets as $k => $widget) {
            if ($widget instanceof CmsUiEditionFormWidgetInterface) {
                $this->blockWidgets[$k] = $widget;
            }
        }
        return $this;
    }

    public function updateEntity(): static
    {
        $widgets = [];
        foreach ($this->getBlockWidgets() as $widget) {
            $widgets[] = $widget;
            $widgets = array_merge($widgets, $widget->getDescendants());
        }
        foreach ($widgets as $k => $widget) {
            $widget->updateEntity();
        }
        return $this;
    }

    public function render(): string
    {
        return $this->getTwig()->render(
            '@CmsUi/Widget/CompositeEditionFormWidget/widget.twig',
            [
                'block_widgets' => $this->getBlockWidgets(),
            ]
        );
    }

    public function updateValidationResult(): static
    {
        $widgets = [];
        foreach ($this->getBlockWidgets() as $widget) {
            $widgets[] = $widget;
            $widgets = array_merge($widgets, $widget->getDescendants());
        }
        foreach ($widgets as $k => $widget) {
            $widget->updateValidationResult();
        }
        return $this;
    }

}