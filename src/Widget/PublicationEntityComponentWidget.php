<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Twig\Environment;
use Vitya\CmsComponent\EntityComponent\PublicationEntityComponent;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Frontend\WebFrontend;

class PublicationEntityComponentWidget extends AbstractCmsUiEditionFormWidget
{
    private $entityComponents = [];
    private $twig = null;
    private $user = null;
    private $webFrontend = null;

    public function __construct(Environment $twig, WebFrontend $web_frontend)
    {
        parent::__construct();
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    public function render(): string
    {
        $components = [];
        $validation_result = $this->getWidgetValidationResult();
        $previous_value = [];
        $component_counter = -1;
        foreach ($this->entityComponents as $k => $entity_component) {
            $component_counter++;
            $component_value_array = [
                'label' => $entity_component['label'],
                'publishable' => $entity_component['component']->isPublishable(),
                'start_uts' => $entity_component['component']->getStartUts(),
                'end_uts' => $entity_component['component']->getEndUts(),
                'error' => false,
                'base_name' => $this->getBaseName(),
            ];
            if (isset($previous_value[$k])) {
                $component_value_array['publishable'] = (isset($previous_value[$k]['publishable']) ? $previous_value[$k]['publishable'] : false);
                $component_value_array['start_uts'] = (isset($previous_value[$k]['start_uts']) ? $previous_value[$k]['start_uts'] : null);
                $component_value_array['end_uts'] = (isset($previous_value[$k]['end_uts']) ? $previous_value[$k]['end_uts'] : null);
                $component_value_array['error'] = (isset($previous_value[$k]['error']) ? $previous_value[$k]['error'] : false);
            }
            $components[$component_counter] = $component_value_array;
        }
        return $this->twig->render(
            '@CmsUi/Widget/PublicationEntityComponentWidget/widget.twig',
            [
                'components' => $components,
                'html_error_description' => (null !== $validation_result ? $validation_result->getHtmlErrorDescription() : ''),
            ]
        );
    }

    public function updateEntity(): static
    {
        $main_request = $this->webFrontend->getMainServerRequest();
        $params = $main_request->getParsedBody();
        $component_counter = -1;
        foreach ($this->entityComponents as $entity_component_array) {
            $component_counter++;
            $entity_component = $entity_component_array['component'];
            if (false === $entity_component->canBeModified($this->user)) {
                continue;
            }
            $value = $entity_component->get();
            if (
                isset($params[$this->getBaseName() . '_' . $component_counter . '_publishable']) 
                && '1' === $params[$this->getBaseName() . '_' . $component_counter . '_publishable']
            ) {
                $value['publishable'] = true;
            } else {
                $value['publishable'] = false;
            }
            $value['start_uts'] = null;
            if (isset($params[$this->getBaseName() . '_' . $component_counter . '_start_uts'])) {
                $start_time_string = trim((string) $params[$this->getBaseName() . '_' . $component_counter . '_start_uts']);
                if ('' !== $start_time_string) {
                    $start_uts = strtotime($start_time_string);
                    if (false !== $start_uts) {
                        $value['start_uts'] = $start_uts;
                    }
                }
            }
            $value['end_uts'] = null;
            if (isset($params[$this->getBaseName() . '_' . $component_counter . '_end_uts'])) {
                $end_time_string = trim((string) $params[$this->getBaseName() . '_' . $component_counter . '_end_uts']);
                if ('' !== $end_time_string) {
                    $end_uts = strtotime($end_time_string);
                    if (false !== $end_uts) {
                        $value['end_uts'] = $end_uts;
                    }
                }
            }
            $entity_component->set($value);
        }
        return $this;
    }

    public function addEntityComponent(PublicationEntityComponent $entity_component, string $id, string $label): static
    {
        $this->entityComponents[$id] = [
            'label' => $label,
            'component' => $entity_component,
        ];
        return $this;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getWebFrontend(): WebFrontend
    {
        return $this->webFrontend;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function updateValidationResult(): static
    {
        $widget_validation_result = new WidgetValidationResult();
        $previous_value = [];
        foreach ($this->entityComponents as $k => $entity_component_array) {
            $entity_component = $entity_component_array['component'];
            $previous_value[$k] = $entity_component->get();
            $previous_value[$k]['error'] = false;
            if (false === $entity_component->isInAValidState()) {
                $widget_validation_result->setError(true);
                $widget_validation_result->addHtmlErrorMessage('Publication dates are incorrect.');
                $widget_validation_result->setHtmlErrorDescription('Please check the dates: publication can\'t end before it starts.');
                $previous_value[$k]['error'] = true;
            }
        }
        $this->setWidgetValidationResult($widget_validation_result);
        return $this;
    }

}
