<?php

/*
 * Copyright 2023, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;

class AbstractCmsUiEditionFormWidget implements CmsUiEditionFormWidgetInterface
{
    private $baseName = 'unnamed';
    private $widgetValidationResult = null;

    public function __construct()
    {
    }

    public function render(): string
    {
        return '';
    }

    public function getBaseName(): string
    {
        return $this->baseName;
    }

    public function setBaseName(string $base_name): static
    {
        if (false === preg_match('/^[a-z0-9\-]+$/', $base_name)) {
            throw new Exception('Invalid base name for widget.');
        }
        $this->baseName = $base_name;
        return $this;
    }

    public function setWidgetValidationResult(WidgetValidationResultInterface $widget_validation_result = null): static
    {
        $this->widgetValidationResult = $widget_validation_result;
        return $this;
    }

    public function updateEntity(): static
    {
        return $this;
    }

    public function getChildren(): array
    {
        return [];
    }

    public function getDescendants(): array
    {
        return [];
    }

    public function updateValidationResult(): static
    {
        return $this;
    }

    public function getWidgetValidationResult(): ?WidgetValidationResultInterface
    {
        return $this->widgetValidationResult;
    }

}
