<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Psr\Http\Message\UriInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsComponent\EntityComponent\EntityComponentInterface;
use Vitya\CmsComponent\EntityComponent\L10nEntityComponent;
use Vitya\CmsApplication\DatumEditionFormWidget\DatumEditionFormWidgetInterface;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityWidgetBuilder
{
    private $dependencyInjector = null;
    private $localeCatalog = null;

    public function __construct(DependencyInjectorInterface $dependency_injector, LocaleCatalog $locale_catalog)
    {
        $this->dependencyInjector = $dependency_injector;
        $this->localeCatalog = $locale_catalog;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getLocaleCatalog(): LocaleCatalog
    {
        return $this->localeCatalog;
    }

    public function getSanitizeEditionFormDatumWidgetsDescription(array $description): array
    {
        foreach ($description as $k => $widget_description) {
            if (false === isset($widget_description['class']) || false === is_string($widget_description['class'])) {
                throw new Exception('A widget class name must be provided (element "' . $k . '" of the description).');
            }
            if (false === isset($widget_description['localized'])) {
                $description[$k]['localized'] = false;
            } else {
                $description[$k]['localized'] = (bool) $widget_description['localized'];
            }
            if (false === isset($widget_description['data'])) {
                $description[$k]['data'] = [];
            }
            if (false === isset($widget_description['options'])) {
                $description[$k]['options'] = [];
            }
        }
        return $description;
    }

    public function createWidgetsFromDescription(array $description, EntityComponentInterface $root_component, UserInterface $user, UriInterface $origin_uri = null): array
    {
        $widgets = [];
        $description = $this->getSanitizeEditionFormDatumWidgetsDescription($description);
        $locales = $this->localeCatalog->getLocales();
        foreach ($description as $k => $widget_description) {
            $localized = false;
            foreach ($widget_description['data'] as $datum_id) {
                $datum = $root_component->getChild($datum_id);
                if ($datum instanceof L10nEntityComponent) {
                    $localized = true;
                }
            }
            if ($localized) {
                $l10n_component_widget = $this->dependencyInjector->make('Vitya\CmsApplication\Widget\L10nEntityComponentWidget');
                foreach ($locales as $locale) {
                    $widget = $this->dependencyInjector->make($widget_description['class']);
                    if (false === $widget instanceof DatumEditionFormWidgetInterface) {
                        throw new Exception(get_class($widget) . ' does not implement DatumEditionFormWidgetInterface.');
                    }
                    $data = [];
                    foreach ($widget_description['data'] as $datum_id) {
                        $data[] = $root_component->getChild($datum_id)->getChild($locale->getId());
                    }
                    $widget
                        ->setData($data)
                        ->setUser($user)
                        ->setOptions($widget_description['options'])
                        ->addAnnotation($locale->getId())
                    ;
                    $l10n_component_widget->addDatumWidget($locale->getId(), $widget);
                }
                $widgets[] = $l10n_component_widget;
            } else {
                $widget = $this->dependencyInjector->make($widget_description['class']);
                if (false === $widget instanceof DatumEditionFormWidgetInterface) {
                    throw new Exception(get_class($widget) . ' does not implement DatumEditionFormWidgetInterface.');
                }
                $data = [];
                foreach ($widget_description['data'] as $datum_id) {
                    $data[] = $root_component->getChild($datum_id);
                }
                $widget
                    ->setData($data)
                    ->setUser($user)
                    ->setOptions($widget_description['options'])
                ;
                $widgets[] = $widget;
            }
        }
        return $widgets;
    }

    public function generateBaseName(string $series_name, int $rank): string {
        return 'w' . md5($series_name . '+' . $rank);
    }

}
