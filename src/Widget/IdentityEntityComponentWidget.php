<?php

/*
 * Copyright 2022, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsComponent\EntityComponent\IdentityEntityComponent;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Widget\WidgetValidationResult;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Frontend\WebFrontend;
use Vitya\Component\Route\RouterInterface;

class IdentityEntityComponentWidget extends AbstractCmsUiEditionFormWidget
{
    private $entityComponent = null;
    private $cmsUi = null;
    private $router = null;
    private $twig = null;
    private $webFrontend = null;
    private $user = null;
    private $originUri = null;

    public function __construct(CmsUi $cms_ui, RouterInterface $router, Environment $twig, WebFrontend $web_frontend)
    {
        parent::__construct();
        $this->cmsUi = $cms_ui;
        $this->router = $router;
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
    }

    public function render(): string
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('An entity component must be set before rendering the widget.');
        }
        if (false === $entity_component->canBeViewed($this->user)) {
            return '';
        }
        $validation_result = $this->getWidgetValidationResult();
        $edition_form_uri = $this->cmsUi->decoratePageUri(
            $this->router->createUri(
                'cms-ui-identity-component-password-edition-form',
                [
                    'component_address' => preg_replace('@^/@', '', $entity_component->getAddress()),
                ],
                [
                    'origin_url' => (string) $this->originUri,
                ]
            )
        );
        return $this->twig->render(
            '@CmsUi/Widget/IdentityEntityComponentWidget/widget.twig',
            [
                'base_name' => $this->getBaseName(),
                'username' => $entity_component->getUsername(),
                'password' => $entity_component->getPassword(),
                'can_be_modified' => $entity_component->canBeModified($this->user),
                'can_have_password_modified' => $entity_component->canHavePasswordModified($this->user),
                'fully_created' => $entity_component->getEntity()->isFullyCreated(),
                'edition_form_uri' => $edition_form_uri,
                'error' => (null !== $validation_result ? $validation_result->isError() : false),
                'html_error_description' => (null !== $validation_result ? $validation_result->getHtmlErrorDescription() : ''),
            ]
        );
    }

    public function updateEntity(): static
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('No entity component set.');
        }
        if (false === $entity_component->canBeModified($this->user)) {
            return $this;
        }
        $base_name = $this->getBaseName();
        $main_request = $this->webFrontend->getMainServerRequest();
        $params = $main_request->getParsedBody();
        $username = '';
        if (isset($params[$base_name])) {
            $username = (string) $params[$base_name];
        }
        $value = $this->getEntityComponent()->get();
        $value['username'] = $username;
        $this->getEntityComponent()->set($value);
        return $this;
    }

    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function setEntityComponent(IdentityEntityComponent $entity_component): static
    {
        $this->entityComponent = $entity_component;
        return $this;
    }

    public function getEntityComponent(): ?IdentityEntityComponent
    {
        return $this->entityComponent;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function getOriginUri(): ?UriInterface
    {
        return $this->originUri;
    }

    public function setOriginUri(UriInterface $origin_uri): static
    {
        $this->originUri = $origin_uri;
        return $this;
    }

    public function updateValidationResult(): static
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('No entity component set.');
        }
        $widget_validation_result = new WidgetValidationResult();
        if (false === $entity_component->isInAValidState()) {
            $widget_validation_result->setError(true);
            $validation_errors = $entity_component->getValidationErrors();
            if (in_array(IdentityEntityComponent::VALIDATION_ERROR_EMPTY_USERNAME, $validation_errors)) {
                $widget_validation_result->addHtmlErrorMessage('<em>Username</em> cannot be empty.');
                $widget_validation_result->setHtmlErrorDescription('Please enter a username.');
            }
        }
        if ($entity_component->usernameIsAlreadyInUse()) {
            $widget_validation_result->addHtmlErrorMessage('Username <em>' . htmlspecialchars($entity_component->getUsername()) . '</em> is already in use.');
            $widget_validation_result->setHtmlErrorDescription('Please choose a different username.');
        }
        $this->setWidgetValidationResult($widget_validation_result);
        return $this;
    }

}
