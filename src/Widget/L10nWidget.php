<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsApplication\CmsUi;
use Vitya\Component\Frontend\WebFrontend;
use Vitya\Component\L10n\Locale;
use Vitya\Component\L10n\LocaleCatalog;

class L10nWidget implements CmsUiPageWidgetInterface
{
    private $cmsUi = null;
    private $localeCatalog = null;
    private $twig = null;
    private $webFrontend = null;
    private $activeLocale = null;

    public function __construct(
        CmsUi $cms_ui,
        LocaleCatalog $locale_catalog,
        Environment $twig,
        WebFrontend $web_frontend
    ) {
        $this->cmsUi = $cms_ui;
        $this->localeCatalog = $locale_catalog;
        $this->twig = $twig;
        $this->webFrontend = $web_frontend;
        $this->activeLocale = $locale_catalog->getDefaultLocale();
        $params = $web_frontend->getMainServerRequest()->getQueryParams();
        if (isset($params['locale'])) {
            $this->activeLocale = $locale_catalog->getLocale($params['locale']);
        }
        $this->cmsUi->setEnv('locale', $this->activeLocale->getId());
    }

    public function render(): string
    {
        $locales = $this->localeCatalog->getLocales();
        if (count($locales) < 2) {
            return '';
        }
        $items = [];
        $raw_url = $this->cmsUi->decoratePageUri(
            $this->webFrontend->getMainServerRequest()->getUri()->withQuery('')
        );
        parse_str($raw_url->getQuery(), $query_params);
        foreach ($locales as $locale) {
            $query_params['locale'] = $locale->getId();
            $items[$locale->getId()] = [
                'id' => $locale->getId(),
                'name' => $locale->getName(),
                'active' => ($locale->getId() === $this->activeLocale->getId()),
                'url' => $raw_url->withQuery(http_build_query($query_params)),
            ];
        }
        return $this->twig->render(
            '@CmsUi/Widget/L10nWidget/l10n-widget.twig',
            [
                'items' => $items,
            ]
        );
    }

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface
    {
        if (null === $uri) {
            return null;
        }
        $locales = $this->localeCatalog->getLocales();
        if (count($locales) < 2) {
            return $uri;
        }
        parse_str($uri->getQuery(), $query_params);
        $query_params['locale'] = $this->activeLocale->getId();
        $uri = $uri->withQuery(http_build_query($query_params));
        return $uri;
    }

    public function getActiveLocale(): Locale
    {
        return $this->activeLocale;
    }

}
