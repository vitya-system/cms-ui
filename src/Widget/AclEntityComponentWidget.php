<?php

/*
 * Copyright 2022, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Widget;

use Exception;
use Psr\Http\Message\UriInterface;
use Twig\Environment;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsComponent\EntityComponent\AclEntityComponent;
use Vitya\CmsApplication\CmsUi;
use Vitya\Component\Acl\AclInterface;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Route\RouterInterface;

class AclEntityComponentWidget extends AbstractCmsUiEditionFormWidget
{
    private $entityComponent = null;
    private $cmsUi = null;
    private $router = null;
    private $twig = null;
    private $user = null;
    private $originUri = null;
    private $permissionLabels = [];

    public function __construct(CmsUi $cms_ui, RouterInterface $router, Environment $twig)
    {
        parent::__construct();
        $this->cmsUi = $cms_ui;
        $this->router = $router;
        $this->twig = $twig;
    }

    public function render(): string
    {
        $entity_component = $this->getEntityComponent();
        if (null === $entity_component) {
            throw new Exception('An entity component must be set before rendering the widget.');
        }
        $acl = $entity_component->getComponentAcl();
        if (null === $acl) {
            return '';
        }
        $user_groups = $user = $entity_component->getAuthenticationService()->getUserProvider($entity_component->getAuthenticationRealm())->getUserGroups();
        $permissions = $acl->getPermissions();
        $regrouped_permissions = [];
        foreach ($permissions as $permission) {
            $k = $permission['type'] . ':' . $permission['identifier'];
            if (!isset($regrouped_permissions[$k])) {
                $type_human_name = '?';
                $label = '';
                if ($permission['type'] === AclInterface::TYPE_ALL) {
                    $type_human_name = 'all users';
                    $label = '';
                } elseif ($permission['type'] === AclInterface::TYPE_GROUP) {
                    $type_human_name = 'group';
                    if (isset($user_groups[$permission['identifier']])) {
                        $label = $user_groups[$permission['identifier']];
                    } else {
                        $label = $permission['identifier'];
                    }
                } elseif ($permission['type'] === AclInterface::TYPE_USER) {
                    $type_human_name = 'user';
                    $user = $entity_component->getAuthenticationService()->getUserProvider($entity_component->getAuthenticationRealm())->loadUserByIdentifier($permission['identifier']);
                    if ($user instanceof AbstractEntity) {
                        $label = $user->getTitle();
                    } else {
                        $label = $permission['identifier'];
                    }
                }
                $regrouped_permissions[$k] = [
                    'type' => $permission['type'],
                    'type_human_name' => $type_human_name,
                    'label' => $label,
                    'identifier' => $permission['identifier'],
                    'permissions' => [],
                ];
            }
            $regrouped_permissions[$k]['permissions'][$permission['permission']] = $permission['permission'];
        }
        ksort($regrouped_permissions);
        $edition_form_uri = $this->cmsUi->decoratePageUri(
            $this->router->createUri(
                'cms-ui-acl-component-edition-form',
                [
                    'component_address' => preg_replace('@^/@', '', $entity_component->getAddress()),
                ],
                [
                    'origin_url' => (string) $this->originUri,
                ]
            )
        );
        $permission_labels = [];
        foreach ($entity_component->getPossiblePermissions() as $possible_permission) {
            if (isset($this->permissionLabels[$possible_permission]) && is_string($this->permissionLabels[$possible_permission])) {
                $permission_labels[$possible_permission] = $this->permissionLabels[$possible_permission];
            } else {
                $permission_labels[$possible_permission] = $possible_permission;
            }
        }
        return $this->twig->render(
            '@CmsUi/Widget/AclEntityComponentWidget/widget.twig',
            [
                'permission_labels' => $permission_labels,
                'items' => $regrouped_permissions,
                'edition_form_uri' => $edition_form_uri,
                'can_be_modified' => $entity_component->canBeModified($this->user),
                'fully_created' => $entity_component->getEntity()->isFullyCreated(),
            ]
        );
    }

    public function updateEntity(): static
    {
        return $this;
    }

    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    public function getOriginUri(): ?UriInterface
    {
        return $this->originUri;
    }

    public function setOriginUri(UriInterface $origin_uri): static
    {
        $this->originUri = $origin_uri;
        return $this;
    }

    public function setEntityComponent(AclEntityComponent $entity_component): static
    {
        $this->entityComponent = $entity_component;
        return $this;
    }

    public function getEntityComponent(): ?AclEntityComponent
    {
        return $this->entityComponent;
    }

    public function setPermissionLabels(array $permission_labels): static
    {
        $this->permissionLabels = $permission_labels;
        return $this;
    }

    public function getPermissionLabels(): array
    {
        return $this->permissionLabels;
    }

}
