<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Session;

use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\Component\Session\HtmlFlashMessage;

class CouldNotSaveEntityHtmlFlashMessage extends HtmlFlashMessage
{
    private $entityClass = '';
    private $entityId = 0;
    private $entityContent = [];

    public function __construct(string $html_title, string $html_body, AbstractEntity $entity)
    {
        parent::__construct($html_title, $html_body);
        $this->entityClass = get_class($entity);
        $this->entityId = $entity->getId();
        $this->entityContent = $entity->getRootComponent()->get();
    }

    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    public function getEntityId(): int
    {
        return $this->entityId;
    }

    public function getEntityContent(): array
    {
        return $this->entityContent;
    }

}
