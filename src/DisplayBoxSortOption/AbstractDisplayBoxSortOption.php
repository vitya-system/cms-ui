<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DisplayBoxSortOption;

use Exception;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\Component\Authentication\UserInterface;

abstract class AbstractDisplayBoxSortOption implements DisplayBoxSortOptionInterface
{
    private $id = '';
    private $entity = null;
    private $options = [];
    private $user = null;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): DisplayBoxSortOptionInterface
    {
        $this->id = $id;
        return $this;
    }

    public function setEntity(AbstractEntity $entity): DisplayBoxSortOptionInterface
    {
        $this->entity = $entity;
        return $this;
    }

    public function setUser(UserInterface $user): DisplayBoxSortOptionInterface
    {
        $this->user = $user;
        return $this;
    }

    public function getLabel(): string
    {
        return $this->options['label'];
    }

    public function getAscLabel(): string
    {
        return $this->options['asc_label'];
    }

    public function getDescLabel(): string
    {
        return $this->options['desc_label'];
    }

    public function getDefaultDirection(): string
    {
        return $this->options['default_direction'];
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): DisplayBoxSortOptionInterface
    {
        $options = array_merge($this->getDefaultOptions(), $options);
        $this->assertValidOptions($options);
        $this->options = $options;
        return $this;
    }

    public function getEntity(): AbstractEntity
    {
        if (null === $this->entity) {
            throw new Exception('No entity attached to this display box sort option.');
        }
        return $this->entity;
    }

    public function getUser(): UserInterface
    {
        if (null === $this->user) {
            throw new Exception('No user attached to this display box sort option.');
        }
        return $this->user;
    }

    public function getDefaultOptions(): array
    {
        return [
            'label' => '',
            'asc_label' => '',
            'desc_label' => '',
            'default_direction' => 'ASC',
        ];
    }

    public function getEntityListModifiers(string $direction): array
    {
        return [];
    }

    public function assertValidOptions(array $options): AbstractDisplayBoxSortOption
    {
        if (false === is_string($options['label'])) {
            throw new Exception('Sort option label must be defined as a string.');
        }
        if (false === is_string($options['asc_label'])) {
            throw new Exception('Sort option asc_label must be defined as a string.');
        }
        if (false === is_string($options['desc_label'])) {
            throw new Exception('Sort option desc_label must be defined as a string.');
        }
        if (false === is_string($options['default_direction'])) {
            throw new Exception('Sort option default_direction must be defined as a string.');
        }
        if (false === in_array($options['default_direction'], ['ASC', 'DESC'])) {
            throw new Exception('Sort option default_direction must be ASC or DESC.');
        }
    return $this;
    }

}
