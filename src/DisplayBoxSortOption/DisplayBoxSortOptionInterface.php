<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DisplayBoxSortOption;

use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\Component\Authentication\UserInterface;

interface DisplayBoxSortOptionInterface
{
    public function getId(): string;

    public function setId(string $id): DisplayBoxSortOptionInterface;

    public function setEntity(AbstractEntity $entity): DisplayBoxSortOptionInterface;

    public function setUser(UserInterface $user): DisplayBoxSortOptionInterface;

    public function getLabel(): string;

    public function getAscLabel(): string;

    public function getDescLabel(): string;

    public function getDefaultDirection(): string;

    public function getOptions(): array;

    public function setOptions(array $options): DisplayBoxSortOptionInterface;

    public function getEntityListModifiers(string $direction): array;

}
