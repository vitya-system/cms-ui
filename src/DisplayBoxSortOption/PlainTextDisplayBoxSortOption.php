<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DisplayBoxSortOption;

use Exception;
use Vitya\CmsComponent\Datum\PlainTextDatum;
use Vitya\CmsComponent\EntityListModifier\PlainTextDatumSort;

class PlainTextDisplayBoxSortOption extends AbstractDisplayBoxSortOption
{
    public function getDefaultOptions(): array
    {
        $default_options = [
            'label' => '?',
            'asc_label' => 'A > Z',
            'desc_label' => 'Z > A',
            'default_direction' => 'ASC',
            'datum' => '',
        ];
        return array_merge(parent::getDefaultOptions(), $default_options);
    }

    public function getEntityListModifiers(string $direction): array
    {
        $options = $this->getOptions();
        $entity_list_sort_option = new PlainTextDatumSort($options['datum'], $direction);
        $entity_list_sort_options = [$entity_list_sort_option];
        return $entity_list_sort_options;
    }

    public function assertValidOptions(array $options): AbstractDisplayBoxSortOption
    {
        if (false === is_string($options['datum'])) {
            throw new Exception('Option "datum" must be defined as a string.');
        }
        $datum = $this->getEntity()->getFromRelativeAddress($options['datum']);
        if (null === $datum) {
            throw new Exception('No datum found at relative address ' . $options['datum'] . '.');
        }
        if (false === $datum instanceof PlainTextDatum) {
            throw new Exception('Datum must be an instance of PlainTextDatum.');
        }
        return parent::assertValidOptions($options);
    }

}
