<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\DisplayBoxSortOption;

use Vitya\CmsComponent\EntityListModifier\EntitySort;

class LastModificationTimeDisplayBoxSortOption extends AbstractDisplayBoxSortOption
{
    public function getDefaultOptions(): array
    {
        $default_options = [
            'label' => 'last modification time',
            'asc_label' => 'older first',
            'desc_label' => 'newer first',
            'default_direction' => 'DESC',
        ];
        return array_merge(parent::getDefaultOptions(), $default_options);
    }

    public function getEntityListModifiers(string $direction): array
    {
        $entity_list_sort_options = [];
        if ('ASC' === $direction) {
            $entity_list_sort_options[] = new EntitySort('last_modification_time', 'ASC');
            $entity_list_sort_options[] = new EntitySort('id', 'ASC');
        } elseif ('DESC' === $direction) {
            $entity_list_sort_options[] = new EntitySort('last_modification_time', 'DESC');
            $entity_list_sort_options[] = new EntitySort('id', 'DESC');
        }
        return $entity_list_sort_options;
    }

}
