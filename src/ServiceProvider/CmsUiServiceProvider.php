<?php

/*
 * Copyright 2020, 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\ServiceProvider;

use Vitya\CmsApplication\CmsUi;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;

class CmsUiServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'cms_ui';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\CmsApplication\CmsUi'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        return new CmsUi(
            $service_container->get('authentication'),
            $service_container->get('l10n'),
            $service_container->get('dependency_injector'),
            $service_container->getParameters(),
            $service_container['cms_ui__url_path_prefix']
        );
    }

}
