<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication;

use Exception;
use Psr\Http\Message\UriInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\L10n\Locale;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Service\DependencyInjectorInterface;

class CmsUi
{
    private $authn = null;
    private $cmsUiUrlPathPrefix = '';
    private $localeCatalog = null;
    private $dependencyInjector = null;
    private $appParams = [];
    private $pageZones = null;
    private $env = [];
    private $l10nWidget = null;

    public function __construct(
        AuthenticationService $authn,
        LocaleCatalog $locale_catalog,
        DependencyInjectorInterface $dependency_injector,
        array $app_params,
        string $cms_ui_url_path_prefix
    ) {
        $this->authn = $authn;
        $this->cmsUiUrlPathPrefix = $cms_ui_url_path_prefix;
        $this->localeCatalog = $locale_catalog;
        $this->dependencyInjector = $dependency_injector;
        $this->appParams = $app_params;
    }

    public function getAuthenticationService(): AuthenticationService
    {
        return $this->authn;
    }

    public function getCmsUiUrlPathPrefix(): string
    {
        return $this->cmsUiUrlPathPrefix;
    }

    public function getLocaleCatalog(): LocaleCatalog
    {
        return $this->localeCatalog;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getAppParams(): array
    {
        return $this->appParams;
    }

    public function initPageWidgets()
    {
        $this->pageZones = [];
        $this->l10nWidget = $this->dependencyInjector->make('Vitya\CmsApplication\Widget\L10nWidget');
        $this->pageZones['sidebar'] = [];
        $this->pageZones['sidebar'][] = $this->l10nWidget;
        $this->pageZones['sidebar'][] = $this->dependencyInjector->make('Vitya\CmsApplication\Widget\NavigationWidget');
    }

    public function getPageWidgets(string $zone = null): array
    {
        if (null === $this->pageZones) {
            $this->initPageWidgets();
        }
        if (null !== $zone) {
            if (false === isset($this->pageZones[$zone])) {
                throw new Exception('Zone "' . $zone . '" is not defined.');
            }
            return $this->pageZones[$zone];
        }
        $widget_list = [];
        foreach ($this->pageZones as $page_zone) {
            foreach ($page_zone as $widget) {
                $widget_list[] = $widget;
            }
        }
        return $widget_list;
    }

    public function decoratePageUri(UriInterface $uri = null): ?UriInterface
    {
        if (null === $uri) {
            return null;
        }
        foreach ($this->getPageWidgets() as $widget) {
            $uri = $widget->decoratePageUri($uri);
        }
        return $uri;
    }

    public function getEnv(string $key): ?string
    {
        if (isset($this->env[$key])) {
            return $this->env[$key];
        }
        return null;
    }

    public function setEnv(string $key, string $value): CmsUi
    {
        $this->env[$key] = $value;
        return $this;
    }

    public function unsetEnv(string $key): CmsUi
    {
        if (isset($this->env[$key])) {
            unset($this->env[$key]);
        }
        return $this;
    }

    public function userIsSysadmin(): bool
    {
        $user = $this->authn->getUser('cms');
        if (null !== $user) {
            if ($user->isOmnipotent()) {
                return true;
            }
            $sysadmin_group = (int) $this->appParams['cms_user_provider__sysadmin_user_group'];
            if ($sysadmin_group > 0 && in_array($sysadmin_group, $user->getGroups())) {
                return true;
            }
        }
        return false;
    }

    public function getWorkingLocale(): Locale
    {
        if (null === $this->pageZones) {
            $this->initPageWidgets();
        }
        $locale_name = (string) $this->getEnv('locale');
        if (false === $this->getLocaleCatalog()->hasLocale($locale_name)) {
            return $this->getLocaleCatalog()->getDefaultLocale();
        }
        return $this->getLocaleCatalog()->getLocale($locale_name);
    }

}
