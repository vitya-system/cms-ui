<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller\Entity;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Controller\CmsUiPageController;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityDuplicationController extends CmsUiPageController
{
    public function entityDuplicationForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $collection_slug,
        int $id
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity?
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $entity = $this->ef()->load($entity_class_name, $id)) {
            $this->notFound();
        }
        if (false === $entity instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($entity) . '" does not extend AbstractEntity.');
        }
        // Is the user allowed to duplicate the entity?
        if (false === $entity->canBeViewed($this->getUser('cms'))) {
            $this->forbidden();
        }
        if (false === $entity->canBeCreated($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Does the entity has a CMS UI integration?
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        // From which index page does the user come?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestQueryParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        // What is the process url?
        $process_uri = $entity_cms_admin_integration->getProcessDuplicationFormUri($entity->getId());
        $process_uri = $cms_ui->decoratePageUri($process_uri);
        // Entity card.
        $card_widget = $entity_cms_admin_integration->makeCardWidget();
        $card_widget
            ->setOriginUri($origin_uri)
            ->setWithLinks(false)
        ;
        // Nonce.
        $nonce = $this->createNonce('entity_duplication_controller_process_entity_duplication_form_' . $entity->getAddress());
        // Go!
        $this->setTitle('Duplicate article?');
        return $this->render(
            '@CmsUi/Controller/Entity/entityDuplicationForm.twig',
            [
                'name' => $entity_cms_admin_integration->getName(),
                'origin_url' => (string) $origin_uri,
                'process_url' => (string) $process_uri,
                'nonce' => $nonce,
                'card_widget' => $card_widget,
            ]
        );
    }

    public function processEntityDuplicationForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $collection_slug,
        int $id
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity?
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $entity = $this->ef()->load($entity_class_name, $id)) {
            $this->notFound();
        }
        if (false === $entity instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($entity) . '" does not extend AbstractEntity.');
        }
        // Is the user allowed to duplicate the entity?
        if (false === $entity->canBeViewed($this->getUser('cms'))) {
            $this->forbidden();
        }
        if (false === $entity->canBeCreated($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Does the entity has a CMS UI integration?
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        // Where should we redirect the user to after processing?
        $origin_uri = $uri_factory->createUri('/');
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestBodyParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        // Validate the nonce.
        $nonce = $this->getMainRequestBodyParam('nonce', '');
        $this->assertValidNonce('entity_duplication_controller_process_entity_duplication_form_' . $entity->getAddress(), $nonce);
        $locale = $cms_ui->getWorkingLocale();
        // Save the entity.
        $entity->create();
        $this->addHtmlFlashMessage(
            'success',
            htmlspecialchars(ucfirst($entity_cms_admin_integration->getName())) . ' duplicated',
            '<p>' . htmlspecialchars(ucfirst($entity_cms_admin_integration->getName())) . ' <em>' . htmlspecialchars($entity_cms_admin_integration->getTitle($locale->getId())) . '</em> was successfully duplicated.</p>'
        );
        // Redirect the user.
        if (null !== $origin_uri) {
            $this->seeOther($origin_uri);
        }
        $this->notFound();
    }

}
