<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller\Entity;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Throwable;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Controller\CmsUiPageController;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\CmsApplication\Session\CouldNotSaveEntityHtmlFlashMessage;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityEditionController extends CmsUiPageController
{
    public function entityEditionForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        LocaleCatalog $locale_catalog,
        UriFactoryInterface $uri_factory,
        string $collection_slug,
        int $id
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity?
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $entity = $this->ef()->load($entity_class_name, $id)) {
            $this->notFound();
        }
        if (false === $entity instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($entity) . '" does not extend AbstractEntity.');
        }
        // Does the entity has a CMS UI integration?
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        // What are the user's permissions?
        if (false === $entity->canBeViewed($this->getUser('cms'))) {
            $this->forbidden();
        }
        $can_be_modified = $entity->canBeModified($this->getUser('cms'));
        // From which index page does the user come?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestQueryParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        // What is the process URL?
        $process_uri = $entity_cms_admin_integration->getProcessEditionFormUri($entity->getId());
        $process_uri = $cms_ui->decoratePageUri($process_uri);
        // Nonce.
        $nonce = $this->createNonce('entity_edition_controller_process_entity_edition_form_' . $entity->getAddress());
        // Maybe we tried to save this entity, but its content was invalid.
        $error_messages = [];
        $could_not_save_entity_html_flash_message = null;
        foreach ($this->getAllFlashMessages('error') as $error_message) {
            if ($error_message instanceof CouldNotSaveEntityHtmlFlashMessage) {
                if (get_class($entity) === $error_message->getEntityClass() && $entity->getId() === $error_message->getEntityId()) {
                    $entity->getRootComponent()->set($error_message->getEntityContent());
                }
                $could_not_save_entity_html_flash_message = $error_message;
            } 
            $error_messages[] = $error_message;
        }
        foreach ($error_messages as $error_message) {
            $this->getSession()->getFlashMessageQueue('error')->push($error_message);
        }
        // Create edition widgets.
        $widget_zones = $entity_cms_admin_integration->getEditionFormWidgets($origin_uri);
        $widgets = [];
        foreach ($widget_zones as $widget_zone) {
            foreach ($widget_zone as $widget_subzone) {
                foreach ($widget_subzone as $widget) {
                    $widgets[] = $widget;
                    $widgets = array_merge($widgets, $widget->getDescendants());
                }
            }
        }
        // Handle validation information.
        if (null !== $could_not_save_entity_html_flash_message) {
            $html_body = '';
            $html_error_messages = [];
            foreach ($widgets as $widget) {
                $widget->updateValidationResult();
                $widget_validation_result = $widget->getWidgetValidationResult();
                if (null !== $widget_validation_result) {
                    $html_error_messages = array_merge($html_error_messages, $widget_validation_result->getHtmlErrorMessages());
                }
            }
            $html_body = $could_not_save_entity_html_flash_message->getHtmlBody();
            if (false === empty($html_error_messages)) {
                $html_body .= '<ul><li>' . implode('</li><li>', $html_error_messages) . '</li></ul>';
            }
            $could_not_save_entity_html_flash_message->setHtmlBody($html_body);
        }
        // Render.
        $this->setTitle('Edit ' . $entity_cms_admin_integration->getName());
        return $this->render(
            '@CmsUi/Controller/Entity/entityEditionForm.twig',
            [
                'id' => $entity->getId(),
                'name' => $entity_cms_admin_integration->getName(),
                'can_be_modified' => $can_be_modified,
                'origin_url' => (string) $origin_uri,
                'process_url' => (string) $process_uri,
                'nonce' => $nonce,
                'widgets' => [
                    'main' => $widget_zones['main'],
                    'aside' => $widget_zones['aside'],
                ],
                'locale_catalog' => $locale_catalog->getLocales(),
            ]
        );
    }

    public function processEntityEditionForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $collection_slug,
        int $id
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity?
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $entity = $this->ef()->load($entity_class_name, $id)) {
            $this->notFound();
        }
        if (false === $entity instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($entity) . '" does not extend AbstractEntity.');
        }
        // Does the entity has a CMS UI integration?
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        // Does the user have a write permission?
        if (false === $entity->canBeModified($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Where should we redirect the user to after processing?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestBodyParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        $return_uri = $entity_cms_admin_integration->getEditionFormUri($origin_uri);
        $return_uri = $cms_ui->decoratePageUri($return_uri);
        $locale = $cms_ui->getWorkingLocale();
        try {
            // Validate the nonce.
            $nonce = $this->getMainRequestBodyParam('nonce', '');
            $this->assertValidNonce('entity_edition_controller_process_entity_edition_form_' . $entity->getAddress(), $nonce);
            // Validate the user data and update the entity.
            $widget_zones = $entity_cms_admin_integration->getEditionFormWidgets($origin_uri);
            $widgets = [];
            foreach ($widget_zones as $widget_zone) {
                foreach ($widget_zone as $widget_subzone) {
                    foreach ($widget_subzone as $widget) {
                        $widgets[] = $widget;
                        $widgets = array_merge($widgets, $widget->getDescendants());
                    }
                }
            }
            foreach ($widgets as $k => $widget) {
                $widget->updateEntity();
            }
            // Save the entity.
            $entity->setFullyCreated(true);
            $entity->save();
            // Try to complete referenced entities creation.
            foreach ($entity->getReferencesToOtherEntities() as $reference) {
                if (isset($reference['class']) && isset($reference['id'])) {
                    $referenced_entity = $this->ef()->load((string) $reference['class'], (int) $reference['id']);
                    if (false === $referenced_entity instanceof AbstractEntity) {
                        continue;
                    }
                    if (get_class($referenced_entity) === get_class($entity) && $referenced_entity->getId() === $entity->getId()) {
                        continue;
                    }
                    if ($referenced_entity->isFullyCreated()) {
                        continue;
                    }
                    if (false === $referenced_entity->canBeModified($this->getUser('cms'))) {
                        continue;
                    }
                    $referenced_entity->setFullyCreated(true);
                    try {
                        $referenced_entity->save();
                    } catch (Throwable $t) {
                        $this->addHtmlFlashMessage(
                            'warning',
                            'Could not save referenced entity',
                            '<p>Referenced entity <em>' . htmlspecialchars($entity_cms_admin_integration->getTitle($locale->getId())) . '</em> could not be saved.</p>'
                        );    
                    }
                }
            }
            $this->addHtmlFlashMessage(
                'success',
                ucfirst($entity_cms_admin_integration->getName()) . ' saved',
                '<p>' . htmlspecialchars(ucfirst($entity_cms_admin_integration->getName())) . ' <em>' . htmlspecialchars($entity_cms_admin_integration->getTitle($locale->getId())) . '</em> was saved successfully.</p>'
            );
        } catch (Throwable $t) {
            $html_title = 'Could not save ' . $entity_cms_admin_integration->getName();
            $html_body = '';
            if ($entity->isInAValidState()) {
                // Entity is in a valid state, it should have been saved.
                // We want to display the error that occurred.
                $html_body = '<p>' . htmlspecialchars($t->getMessage()) . '</p>';
            }
            $this->getSession()->getFlashMessageQueue('error')->push(new CouldNotSaveEntityHtmlFlashMessage($html_title, $html_body, $entity));
        }
        // Redirect the user.
        if (null !== $return_uri) {
            $this->seeOther($return_uri);
        }
        $this->notFound();
    }

}
