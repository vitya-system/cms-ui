<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller\Entity;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Controller\CmsUiPageController;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityCreationController extends CmsUiPageController
{
    public function processEntityCreationForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $collection_slug
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we make a suitable entity?
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $entity = $this->ef()->make($entity_class_name)) {
            $this->notFound();
        }
        if (false === $entity instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($entity) . '" does not extend AbstractEntity.');
        }
        // Is the user allowed to create an entity?
        if (false === $entity->canBeCreated($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Does the entity has a CMS UI integration?
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        // From which index page does the user come?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestBodyParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        // Validate the nonce.
        $nonce = $this->getMainRequestBodyParam('nonce', '');
        $this->assertValidNonce('entity_creation_controller_process_entity_creation_form', $nonce);
        // Create the entity.
        $entity->setFullyCreated(false);
        $new_entity = $entity->create();
        // Where should we redirect the user?
        $entity_cms_admin_integration = $dependency_injector->make($new_entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($new_entity);
        $return_uri = $entity_cms_admin_integration->getEditionFormUri($origin_uri);
        $return_uri = $cms_ui->decoratePageUri($return_uri);
        if (null !== $return_uri) {
            $this->seeOther($return_uri);
        }
        $this->notFound();
    }

}
