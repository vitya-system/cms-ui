<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller\Entity;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Twig\Environment;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsComponent\Entity\HasATreeStructureInterface;
use Vitya\CmsComponent\Tree\TreeStructure;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Controller\CmsUiPageController;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\Component\Frontend\WebFrontend;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityTreeIndexController extends CmsUiPageController
{
    public function entityTreeIndex(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        Environment $twig,
        string $collection_slug
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $model = $this->ef()->make($entity_class_name)) {
            $this->notFound();
        }
        if (false === $model instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($model) . '" does not extend AbstractEntity.');
        }
        if (false === $model instanceof HasATreeStructureInterface) {
            throw new Exception('Class "' . get_class($model) . '" does not implement HasATreeStructureInterface.');
        }
        if (false === $model->canBeListed($this->getUser('cms'))) {
            $this->forbidden();
        }
        if (false === $model instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($model) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $model_cms_admin_integration = $dependency_injector->make($model->getEntityCmsAdminIntegrationClassName());
        $model_cms_admin_integration->setEntity($model);
        // General variables.
        $can_be_created = $model->canBeCreated($this->getUser('cms'));
        $can_be_reordered = $model->canBeReordered($this->getUser('cms'));
        $this_page_uri = $cms_ui->decoratePageUri($model_cms_admin_integration->getIndexUri());
        // Create items list.
        $entity_list = $model->getEntityList();
        $ids = $entity_list->getIds();
        $entities = $this->ef()->loadMultiple($entity_class_name, $ids);
        $items = [];
        foreach ($entities as $entity) {
            if (null === $entity) {
                continue;
            }
            $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
            $entity_cms_admin_integration->setEntity($entity);
            $card_widget = $entity_cms_admin_integration->makeCardWidget();
            $card_widget
                ->setOriginUri($this_page_uri)
                ->setWithLinks($entity->canBeViewed($this->getUser('cms')))
                ->setDraggable(false)
            ;
            $can_be_deleted = $entity->canBeDeleted($this->getUser('cms'));
            $can_be_duplicated = $can_be_created && $entity->canBeViewed($this->getUser('cms'));
            $deletion_form_uri = $entity_cms_admin_integration->getDeletionFormUri($this_page_uri);
            $deletion_form_uri = $cms_ui->decoratePageUri($deletion_form_uri);
            $duplication_form_uri = $entity_cms_admin_integration->getDuplicationFormUri($this_page_uri);
            $duplication_form_uri = $cms_ui->decoratePageUri($duplication_form_uri);
            $items[$entity->getId()] = [
                'card_widget' => $card_widget,
                'can_be_deleted' => $can_be_deleted,
                'can_be_duplicated' => $can_be_duplicated,
                'duplication_form_url' => (string) $duplication_form_uri,
                'deletion_form_url' => (string) $deletion_form_uri,
            ];
        }
        $process_creation_form_uri = $model_cms_admin_integration->getProcessCreationFormUri($this_page_uri);
        $process_creation_form_uri = $cms_ui->decoratePageUri($process_creation_form_uri);
        $process_index_uri = $model_cms_admin_integration->getProcessIndexUri($this_page_uri);
        $process_index_uri = $cms_ui->decoratePageUri($process_index_uri);
        // Create tree description.
        $collection_info = $model->getCollectionInfo();
        $associative_map = $collection_info->get('tree_structure');
        if (false === is_array($associative_map)) {
            $associative_map = [];
        }
        $tree_structure = new TreeStructure();
        $tree_structure->loadFromAssociativeMap($associative_map, $ids);
        $hierarchy = $tree_structure->getHierarchy();
        $tree_description = $this->getTreeDescription($hierarchy, $items, $twig);
        $associative_array = [];
        foreach ($tree_structure->getAssociativeMap() as $child_id => $parent_id) {
            $associative_array[] = [$child_id, $parent_id];
        }
        // Nonce.
        $creation_nonce = $this->createNonce('entity_creation_controller_process_entity_creation_form');
        $reordering_nonce = $this->createNonce('entity_tree_index_controller_process_entity_tree_index');
        // Go!
        return $this->render(
            '@CmsUi/Controller/Entity/entityTreeIndex.twig',
            [
                'collection_name' => $entity_cms_admin_integration->getCollectionName(),
                'name' => $entity_cms_admin_integration->getName(),
                'this_page_url' => (string) $this_page_uri,
                'can_be_created' => $can_be_created,
                'can_be_reordered' => $can_be_reordered,
                'process_creation_form_url' => (string) $process_creation_form_uri,
                'process_index_url' => (string) $process_index_uri,
                'creation_nonce' => $creation_nonce,
                'reordering_nonce' => $reordering_nonce,
                'items' => $items,
                'nodes_json' => json_encode($tree_description, JSON_PRETTY_PRINT),
                'associative_array_json' => json_encode($associative_array),
            ]
        );
    }

    public function processEntityTreeIndex(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        string $collection_slug
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Retrieve variables.
        $locale = $this->getMainRequestBodyParam('locale');
        // Can we create a suitable entity model?
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $model = $this->ef()->make($entity_class_name)) {
            $this->notFound();
        }
        if (false === $model instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($model) . '" does not extend AbstractEntity.');
        }
        if (false === $model instanceof HasATreeStructureInterface) {
            throw new Exception('Class "' . get_class($model) . '" does not implement HasATreeStructureInterface.');
        }
        if (false === $model->canBeListed($this->getUser('cms'))) {
            $this->forbidden();
        }
        if (false === $model->canBeReordered($this->getUser('cms'))) {
            $this->forbidden();
        }
        if (false === $model instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($model) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $model_cms_admin_integration = $dependency_injector->make($model->getEntityCmsAdminIntegrationClassName());
        $model_cms_admin_integration->setEntity($model);
        // General variables.
        $index_page_uri = $cms_ui->decoratePageUri($model_cms_admin_integration->getIndexUri());
        // Validate the nonce.
        $nonce = $this->getMainRequestBodyParam('nonce', '');
        $this->assertValidNonce('entity_tree_index_controller_process_entity_tree_index', $nonce);
        // Update tree structure.
        $associative_array_json = $this->getMainRequestBodyParam('tree_structure_widget_associative_array', '[]');
        $collection_info = $model->getCollectionInfo();
        $associative_array = json_decode($associative_array_json, true, 512, JSON_THROW_ON_ERROR);
        $associative_map = [];
        if (is_array($associative_array)) {
            foreach ($associative_array as $a) {
                if (2 === count($a)) {
                    $associative_map[(int) $a[0]] = (int) $a[1];
                }
            }
        }
        $collection_info->set('tree_structure', $associative_map);
        $model->saveCollectionInfo();
        $this->addHtmlFlashMessage(
            'success',
            'Structure updated',
            '<p>Structure was saved successfully.</p>'
        );
        // Redirect the user.
        $this->seeOther($index_page_uri);
    }

    public function getTreeDescription(array $hierarchy, array $items, Environment $twig): array
    {
        $tree_description = [];
        foreach ($hierarchy as $parent_id => $children) {
            $tree_description[] = [
                'data' => [
                    'id' => $parent_id,
                    'item_html' => $twig->render(
                        '@CmsUi/Controller/Entity/tree_description_item.twig',
                        [
                            'card_widget' => $items[$parent_id]['card_widget'],
                            'can_be_deleted' => $items[$parent_id]['can_be_deleted'],
                            'deletion_form_url' => $items[$parent_id]['deletion_form_url'],
                        ]
                    ),
                ],
                'nodes' => $this->getTreeDescription($children, $items, $twig),
            ];
        }
        return $tree_description;
    }

}
