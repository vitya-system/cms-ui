<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller\Entity;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Twig\Environment;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Controller\CmsUiPageController;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\CmsApplication\Widget\DisplayBoxWidget;
use Vitya\CmsApplication\Widget\PaginationWidget;
use Vitya\Component\Frontend\WebFrontend;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityIndexController extends CmsUiPageController
{
    public function entityIndex(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        Environment $twig,
        WebFrontend $web_frontend,
        string $collection_slug
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        $entity_class_name = $this->ef()->getClassNameFromCollectionMachineName($collection_slug);
        if (null === $model = $this->ef()->make($entity_class_name)) {
            $this->notFound();
        }
        if (false === $model instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($model) . '" does not extend AbstractEntity.');
        }
        if (false === $model->canBeListed($this->getUser('cms'))) {
            $this->forbidden();
        }
        if (false === $model instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($model) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $model_cms_admin_integration = $dependency_injector->make($model->getEntityCmsAdminIntegrationClassName());
        $model_cms_admin_integration->setEntity($model);
        // General variables.
        $can_be_created = $model->canBeCreated($this->getUser('cms'));
        $this_page_uri = $cms_ui->decoratePageUri($model_cms_admin_integration->getIndexUri());
        // Prepare the filter box.
        $display_box_widget = new DisplayBoxWidget($twig, $model_cms_admin_integration->getIndexUri(), $this_page_uri, $web_frontend);
        $filter_widgets = $model_cms_admin_integration->getDisplayBoxFilterWidgets($model, $this->getUser('cms'));
        $model_cms_admin_integration->giveUniqueBaseNameToDisplayBoxFilterWidgets('filter-box', $filter_widgets);
        $display_box_widget->addSortOptions($model_cms_admin_integration->getDisplayBoxSortOptions($model, $this->getUser('cms')));
        $display_box_widget->addFilterWidgets($filter_widgets);
        $this_page_uri = $display_box_widget->decoratePageUri($this_page_uri);
        // Create items list.
        $entity_list = $model->getEntityList();
        foreach ($display_box_widget->getEntityListModifiers() as $modifier) {
            $entity_list->addModifier($modifier);
        }
        // Prepare the pagination.
        $pagination_widget = new PaginationWidget($twig, $model_cms_admin_integration->getIndexUri(), $this_page_uri, $web_frontend, $entity_list->getCount(), [10, 20, 50]);
        $this_page_uri = $pagination_widget->decoratePageUri($this_page_uri);
        $entity_list->applyPagination($pagination_widget->getPaginationHelper());
        $ids = $entity_list->getIds();
        if ($pagination_widget->getPaginationHelper()->getCurrentPage() > 1 && 0 === count($ids)) {
            $this->notFound();
        }
        $entities = $this->ef()->loadMultiple($entity_class_name, $ids);
        $items = [];
        foreach ($entities as $entity) {
            if (null === $entity) {
                continue;
            }
            $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
            $entity_cms_admin_integration->setEntity($entity);
            $card_widget = $entity_cms_admin_integration->makeCardWidget();
            $card_widget
                ->setOriginUri($this_page_uri)
                ->setWithLinks($entity->canBeViewed($this->getUser('cms')))
            ;
            $can_be_deleted = $entity->canBeDeleted($this->getUser('cms'));
            $can_be_duplicated = $can_be_created && $entity->canBeViewed($this->getUser('cms'));
            $deletion_form_uri = $entity_cms_admin_integration->getDeletionFormUri($this_page_uri);
            $deletion_form_uri = $cms_ui->decoratePageUri($deletion_form_uri);
            $duplication_form_uri = $entity_cms_admin_integration->getDuplicationFormUri($this_page_uri);
            $duplication_form_uri = $cms_ui->decoratePageUri($duplication_form_uri);
            $items[] = [
                'card_widget' => $card_widget,
                'can_be_deleted' => $can_be_deleted,
                'can_be_duplicated' => $can_be_duplicated,
                'duplication_form_url' => (string) $duplication_form_uri,
                'deletion_form_url' => (string) $deletion_form_uri,
            ];
        }
        $process_creation_form_uri = $model_cms_admin_integration->getProcessCreationFormUri($this_page_uri);
        $process_creation_form_uri = $cms_ui->decoratePageUri($process_creation_form_uri);    
        // Nonce.
        $nonce = $this->createNonce('entity_creation_controller_process_entity_creation_form');
        // Go!
        $this->setTitle(mb_ucfirst($model_cms_admin_integration->getCollectionName()));
        return $this->render(
            '@CmsUi/Controller/Entity/entityIndex.twig',
            [
                'collection_name' => $model_cms_admin_integration->getCollectionName(),
                'name' => $model_cms_admin_integration->getName(),
                'this_page_url' => (string) $this_page_uri,
                'can_be_created' => $can_be_created,
                'process_creation_form_url' => (string) $process_creation_form_uri,
                'nonce' => $nonce,
                'display_box_widget' => $display_box_widget,
                'pagination_widget' => $pagination_widget,
                'items' => $items,
            ]
        );
    }

}
