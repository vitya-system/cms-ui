<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller;

use Psr\Http\Message\ResponseInterface;
use Vitya\CmsApplication\CmsUi;
use Vitya\Component\Log\BrowsableLogInterface;
use Vitya\Component\Log\LoggerCollectionInterface;

class SystemController extends CmsUiPageController
{
    const PERMISSION_VIEW = 'r';

    public function logs(CmsUi $cms_ui, LoggerCollectionInterface $logger_collection): ResponseInterface
    {
        $this->requireAuthentication('cms');
        if (false === $cms_ui->userIsSysadmin()) {
            $this->forbidden();
        }
        $selected_log = $this->getMainRequestQueryParam('log', '');
        $available_logs = [];
        foreach ($logger_collection->getAvailableLoggers() as $available_logger) {
            if (false === ($logger_collection->getLogger($available_logger) instanceof BrowsableLogInterface)) {
                continue;
            }
            $uri = $this->uri('cms-ui-system-logs', [], ['log' => $available_logger]);
            $available_logs[$available_logger] = [
                'name' => $available_logger,
                'url' => (string) $cms_ui->decoratePageUri($uri),
                'active' => false,
            ];
        }
        if (count($available_logs) > 0) {
            ksort($available_logs);
            if ('' === $selected_log) {
                $selected_log = array_keys($available_logs)[0];
            }
            if (isset($available_logs[$selected_log])) {
                $available_logs[$selected_log]['active'] = true;
            } else {
                $this->notFound();
            }
        }
        $logger = $logger_collection->getLogger($selected_log);
        $log_is_empty = $logger->logIsEmpty();
        $nb_lines_per_page = 100;
        $first_line = (int) $this->getMainRequestQueryParam('first', '0');
        if ($first_line < 0) {
            $first_line = 0;
        }
        $lines = $logger->getLines($first_line, $nb_lines_per_page + 1);
        if ($first_line > 0 && count($lines) === 0) {
            $this->notFound();
        }
        $eof = (false === isset($lines[$nb_lines_per_page]));
        $prev_url = '';
        $next_url = '';
        if ($first_line > 0) {
            $prev_first_line = $first_line - $nb_lines_per_page;
            if ($prev_first_line < 0) {
                $prev_first_line = 0;
            }
            $uri = $this->uri('cms-ui-system-logs', [], ['log' => $selected_log, 'first' => $prev_first_line]);
            $prev_url = (string) $cms_ui->decoratePageUri($uri);
        }
        if (false === $eof) {
            $next_first_line = $first_line + $nb_lines_per_page;
            $uri = $this->uri('cms-ui-system-logs', [], ['log' => $selected_log, 'first' => $next_first_line]);
            $next_url = (string) $cms_ui->decoratePageUri($uri);
        }
        // Go!
        $this->setTitle('Logs');
        return $this->render('@CmsUi/Controller/SystemController/logs.twig', [
            'available_logs' => $available_logs,
            'lines' => array_reverse($lines),
            'prev_url' => $prev_url,
            'next_url' => $next_url,
            'log_is_empty' => $log_is_empty,
        ]);
    }

}
