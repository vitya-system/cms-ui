<?php

/*
 * Copyright 2024, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Vitya\CmsComponent\Composite\CompositeFactoryInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\CompositeCmsAdminIntegration\HasACompositeCmsAdminIntegrationInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class CompositeListEditionFormWidgetController extends CmsUiPageController
{
    public function block(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        CompositeFactoryInterface $composite_factory
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Retrieve parameters.
        $base_name = $this->getMainRequestQueryParam('base_name', '');
        $entity_class = $this->getMainRequestQueryParam('entity_class', '');
        $entity_id = (int) $this->getMainRequestQueryParam('entity_id', '0');
        $composite_class = $this->getMainRequestQueryParam('composite_class', '');
        // Can we load a suitable entity?
        if (null === $entity = $this->ef()->load($entity_class, $entity_id)) {
            $this->notFound();
        }
        if (false === $entity instanceof AbstractEntity) {
            throw new Exception('Class "' . get_class($entity) . '" does not extend AbstractEntity.');
        }
        // What are the user's permissions?
        if (false === $entity->canBeViewed($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Try to create a composite and to load the associated CMS admin integration.
        $composite = $composite_factory->makeComposite($composite_class);
        $composite->setId('c' . md5(uniqid()));
        $composite->setParent($entity->getRootComponent()); // HACK 
        if (false === $composite instanceof HasACompositeCmsAdminIntegrationInterface) {
            $this->notFound();
        }
        $composite_cms_admin_integration = $dependency_injector->make($composite->getCompositeCmsAdminIntegrationClassName());
        $composite_cms_admin_integration->setComposite($composite);
        $composite_widget = $composite_cms_admin_integration->makeEditionFormWidget();
        return $this->render(
            '@CmsUi/Controller/CompositeListEditionFormWidgetController/block.twig',
            [
                'base_name' => $base_name,
                'composite_id' => $composite->getId(),
                'composite_class' => $composite_class,
                'name' => $composite_cms_admin_integration->getName(),
                'composite_widget' => $composite_widget,
            ]
        );
    }

}