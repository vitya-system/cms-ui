<?php

/*
 * Copyright 2022, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller;

use Exception;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Vitya\CmsComponent\Controller\AbstractController;
use Vitya\CmsComponent\Image\ResponsiveImageSourceInterface;
use Vitya\Component\Image\ImageFactoryInterface;

class ImageController extends AbstractController
{
    public function image(
        ImageFactoryInterface $image_factory,
        ResponseFactoryInterface $response_factory,
        string $sizing,
        string $component_address
    ): ResponseInterface {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity component?
        $component = $this->ao()->getFromAddress('/' . $component_address);
        if (false === $component instanceof ResponsiveImageSourceInterface) {
            $this->notFound();
        }
        // Is the user allowed to view the entity?
        $entity = $component->getEntity();
        if (false === $entity->canBeViewed($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Create the response.
        // The Cache-Control header will be overwritten by the session middleware.
        // We declare the response as public here so that it can still be cached
        // by Vitya's internal caching mechanism.
        $response = $response_factory->createResponse(200)
            ->withHeader('Cache-Control', 'public')
            ->withHeader('ETag', '"' . (string) $entity->getLastModificationUts() . '"')
        ;
        if (false === $this->isModified($this->getRequest(), $response)) {
            return $this->makeNotModifiedResponse($response);
        }
        // Generate final image.
        $image = $image_factory->makeImage();
        $original_width = (int) $component->width;
        $original_height = (int) $component->height;
        if (0 >= $original_height || 0 >= $original_width) {
            throw new Exception('Invalid image component width or height.');
        }
        if ('large' === $sizing) {
            if ($original_width > $original_height) {
                $new_width = 1024;
                $new_height = (int) ((float) $original_height * (float) $new_width / (float) $original_width);
            } else {
                $new_height = 1024;
                $new_width = (int) ((float) $original_width * (float) $new_height / (float) $original_height);
            }
        } elseif ('medium' === $sizing) {
            if ($original_width > $original_height) {
                $new_width = 512;
                $new_height = (int) ((float) $original_height * (float) $new_width / (float) $original_width);
            } else {
                $new_height = 512;
                $new_width = (int) ((float) $original_width * (float) $new_height / (float) $original_height);
            }
        } else {
            throw new Exception('Unrecognized sizing parameter: "' . $sizing . '".');
        }
        $image->loadAndResize($component->getFileStorage()->getLocalFilesystemPath($component->getFilePath()), $new_width, $new_height);
        $mime_type = 'image/webp';
        $response = $response->withHeader('Content-Type', $mime_type)->withBody($image->output($mime_type));
        return $response;
    }

}
