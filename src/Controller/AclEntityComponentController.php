<?php

/*
 * Copyright 2022, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Throwable;
use Vitya\CmsComponent\Entity\EntityInterface;
use Vitya\CmsComponent\EntityComponent\AclEntityComponent;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Controller\CmsUiPageController;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\Component\Acl\AclInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class AclEntityComponentController extends CmsUiPageController
{
    public function editionForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $component_address
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity component?
        $acl_component = $this->ao()->getFromAddress('/' . $component_address);
        if (false === $acl_component instanceof AclEntityComponent) {
            $this->notFound();
        }
        $entity = $acl_component->getEntity();
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        $acl = $acl_component->getComponentAcl();
        // Is the user allowed to modify this ACL?
        if (false === $acl_component->canBeModified($this->getUser('cms'))) {
            $this->forbidden();
        }
        // From which index page does the user come?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestQueryParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        $edition_form_uri = $cms_ui->decoratePageUri($entity_cms_admin_integration->getEditionFormUri($origin_uri));
        // What is the process url?
        $process_uri = $cms_ui->decoratePageUri($this->uri(
            'cms-ui-acl-component-process-edition-form',
            [
                'component_address' => preg_replace('@^/@', '', $acl_component->getAddress()),
            ]
        ));
        // Entity card.
        $card_widget = $entity_cms_admin_integration->makeCardWidget();
        $card_widget
            ->setOriginUri($origin_uri)
            ->setWithLinks(false)
        ;
        // Permission table.
        $permission_table = [
            'all' => [],
            'group' => [],
            'user' => [],
        ];
        $user_groups = $acl_component->getAuthenticationService()->getUserProvider($acl_component->getAuthenticationRealm())->getUserGroups();
        $user_group_labels = [];
        foreach ($user_groups as $group_identifier => $user_group_name) {
            $permission_table['group'][$group_identifier] = [];
            $user_group_labels[$group_identifier] = $user_group_name;
        }
        $available_users = [];
        $user_labels = [];
        $supported_user_class = $acl_component->getAuthenticationService()->getUserProvider($acl_component->getAuthenticationRealm())->getSupportedUserClass();
        try {
            $user_model = $this->ef()->make($supported_user_class);
            if ($user_model instanceof EntityInterface) {
                $user_ids = $user_model->getIds();
                $users = $this->ef()->loadMultiple($supported_user_class, $user_ids);
                foreach ($users as $user_id => $user) {
                    if (null !== $user) {
                        $available_users[$user->getUserIdentifier()] = $user;
                        $user_labels[$user->getUserIdentifier()] = $user->getTitle($cms_ui->getWorkingLocale()->getId()); 
                    }
                }
            }
        } catch (Throwable $t) {
            // Do nothing. The user list won't be populated.
        }
        foreach ($available_users as $available_user) {
            $permission_table['user'][$available_user->getUserIdentifier()] = [];
        }
        foreach ($acl->getPermissions() as $permission) {
            if ($permission['type'] === AclInterface::TYPE_ALL) {
                $permission_table['all'][$permission['permission']] = true;
            } elseif ($permission['type'] === AclInterface::TYPE_GROUP) {
                if (false === isset($permission_table['group'][$permission['identifier']])) {
                    continue;
                }
                $permission_table['group'][$permission['identifier']][$permission['permission']] = true;
            } elseif ($permission['type'] === AclInterface::TYPE_USER) {
                if (false === isset($permission_table['user'][$permission['identifier']])) {
                    continue;
                }
                $permission_table['user'][$permission['identifier']][$permission['permission']] = true;
            }
        }
        $permission_labels = [];
        $permission_labels = $entity_cms_admin_integration->getAclPermissionLabels();
        foreach ($acl_component->getPossiblePermissions() as $possible_permission) {
            if (isset($permission_labels[$possible_permission]) && is_string($permission_labels[$possible_permission])) {
                $permission_labels[$possible_permission] = $permission_labels[$possible_permission];
            } else {
                $permission_labels[$possible_permission] = $possible_permission;
            }
        }
        // Nonce.
        $nonce = $this->createNonce('acl_entity_component_controller_process_entity_edition_form_' . $entity->getAddress());
        // Go!
        $this->setTitle('Modify permissions');
        return $this->render(
            '@CmsUi/Controller/AclEntityComponentController/editionForm.twig',
            [
                'name' => $entity_cms_admin_integration->getName(),
                'origin_url' => (string) $origin_uri,
                'edition_form_url' => (string) $edition_form_uri,
                'process_url' => (string) $process_uri,
                'nonce' => $nonce,
                'card_widget' => $card_widget,
                'permission_labels' => $permission_labels,
                'permission_table' => $permission_table,
                'current_user_identifier' => $this->getUser('cms')->getUserIdentifier(),
                'current_user_groups' => $this->getUser('cms')->getGroups(),
                'user_labels' => $user_labels,
                'user_group_labels' => $user_group_labels,
            ]
        );
    }

    public function processEditionForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $component_address
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity component?
        $acl_component = $this->ao()->getFromAddress('/' . $component_address);
        if (false === $acl_component instanceof AclEntityComponent) {
            $this->notFound();
        }
        $entity = $acl_component->getEntity();
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        // Is the user allowed to modify this ACL?
        if (false === $acl_component->canBeModified($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Where should we redirect the user to after processing?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestBodyParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        $success_return_uri = $cms_ui->decoratePageUri($entity_cms_admin_integration->getEditionFormUri($origin_uri));
        $error_return_uri = $cms_ui->decoratePageUri(
            $this->uri(
                'cms-ui-acl-component-edition-form',
                [
                    'component_address' => preg_replace('@^/@', '', $acl_component->getAddress()),
                ],
                [
                    'origin_url' => (string) $origin_uri,
                ]
            )
        );
        // Validate the nonce.
        $nonce = $this->getMainRequestBodyParam('nonce', '');
        $this->assertValidNonce('acl_entity_component_controller_process_entity_edition_form_' . $entity->getAddress(), $nonce);
        // Update the permissions.
        $html_error_messages = [];
        try {
            $acl_component->getComponentAcl()->resetPermissions();
            $all_params = $this->getMainRequest()->getParsedBody();
            $possible_permissions = $acl_component->getPossiblePermissions();
            $user_groups = $acl_component->getAuthenticationService()->getUserProvider($acl_component->getAuthenticationRealm())->getUserGroups();
            foreach ($all_params as $k => $v) {
                if (substr($k, 0, 2) !== 'p:') {
                    continue;
                }
                $elements = explode(':', $k);
                $elements[2] = (int) $elements[2];
                if ($elements[1] === 'all') {
                    foreach ($possible_permissions as $p) {
                        if ($elements[3] === $p) {
                            $acl_component->getComponentAcl()->addPermission(AclInterface::TYPE_ALL, 0, $p);
                        }
                    }
                } elseif ($elements[1] === 'group') {
                    if (in_array($elements[2], array_keys($user_groups))) {
                        foreach ($possible_permissions as $p) {
                            if ($elements[3] === $p) {
                                $acl_component->getComponentAcl()->addPermission(AclInterface::TYPE_GROUP, $elements[2], $p);
                            }
                        }
                    }
                } elseif ($elements[1] === 'user') {
                    foreach ($possible_permissions as $p) {
                        if ($elements[3] === $p) {
                            $acl_component->getComponentAcl()->addPermission(AclInterface::TYPE_USER, $elements[2], $p);
                        }
                    }
                }
            }
            // Save the entity.
            $entity->save();
            $this->addHtmlFlashMessage(
                'success',
                'Permissions modified',
                '<p>' . htmlspecialchars('Permissions for ' . $entity_cms_admin_integration->getName()) . ' <em>' . htmlspecialchars($entity->getTitle($cms_ui->getWorkingLocale()->getId())) . '</em> were successfully modified.</p>'
            );
        } catch (Throwable $t) {
            $html_body = '<p>' . htmlspecialchars($t->getMessage()) . '</p>';
            $this->addHtmlFlashMessage(
                'error',
                'Could not modify permissions',
                $html_body
            );
            $this->seeOther($error_return_uri);
        }
        // Redirect the user.
        $this->seeOther($success_return_uri);
    }

}
