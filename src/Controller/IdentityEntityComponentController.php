<?php

/*
 * Copyright 2023, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Throwable;
use Vitya\CmsComponent\EntityComponent\IdentityEntityComponent;
use Vitya\CmsApplication\CmsUi;
use Vitya\CmsApplication\Controller\CmsUiPageController;
use Vitya\CmsApplication\EntityCmsAdminIntegration\HasAnEntityCmsAdminIntegrationInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class IdentityEntityComponentController extends CmsUiPageController
{
    public function passwordEditionForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $component_address
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity component?
        $identity_component = $this->ao()->getFromAddress('/' . $component_address);
        if (false === $identity_component instanceof IdentityEntityComponent) {
            $this->notFound();
        }
        $entity = $identity_component->getEntity();
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        // Is the user allowed to modify this password?
        if (false === $identity_component->canHavePasswordModified($this->getUser('cms'))) {
            $this->forbidden();
        }
        // From which index page does the user come?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestQueryParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        $edition_form_uri = $cms_ui->decoratePageUri($entity_cms_admin_integration->getEditionFormUri($origin_uri));
        // What is the process url?
        $process_uri = $cms_ui->decoratePageUri($this->uri(
            'cms-ui-identity-component-process-password-edition-form',
            [
                'component_address' => preg_replace('@^/@', '', $identity_component->getAddress()),
            ]
        ));
        // Entity card.
        $card_widget = $entity_cms_admin_integration->makeCardWidget();
        $card_widget
            ->setOriginUri($origin_uri)
            ->setWithLinks(false)
        ;
        // Nonce.
        $nonce = $this->createNonce('identity_entity_component_controller_process_entity_edition_form_' . $entity->getAddress());
        // Go!
        $this->setTitle('Change password');
        return $this->render(
            '@CmsUi/Controller/IdentityEntityComponentController/passwordEditionForm.twig',
            [
                'name' => $entity_cms_admin_integration->getName(),
                'origin_url' => (string) $origin_uri,
                'edition_form_url' => (string) $edition_form_uri,
                'process_url' => (string) $process_uri,
                'nonce' => $nonce,
                'card_widget' => $card_widget,
            ]
        );
    }

    public function processPasswordEditionForm(
        CmsUi $cms_ui,
        DependencyInjectorInterface $dependency_injector,
        UriFactoryInterface $uri_factory,
        string $component_address
    ): ResponseInterface
    {
        $this->requireAuthentication('cms');
        // Can we load a suitable entity component?
        $identity_component = $this->ao()->getFromAddress('/' . $component_address);
        if (false === $identity_component instanceof IdentityEntityComponent) {
            $this->notFound();
        }
        $entity = $identity_component->getEntity();
        if (false === $entity instanceof HasAnEntityCmsAdminIntegrationInterface) {
            throw new Exception('Class "' . get_class($entity) . '" does not implement HasAnEntityCmsAdminIntegrationInterface.');
        }
        $entity_cms_admin_integration = $dependency_injector->make($entity->getEntityCmsAdminIntegrationClassName());
        $entity_cms_admin_integration->setEntity($entity);
        // Is the user allowed to modify this password?
        if (false === $identity_component->canHavePasswordModified($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Where should we redirect the user to after processing?
        $origin_uri = null;
        $origin_url = '';
        $suggested_origin_url = $this->getMainRequestBodyParam('origin_url', '');
        if ('' !== $suggested_origin_url) {
            $suggested_origin_url = filter_var($suggested_origin_url, FILTER_SANITIZE_URL);
            $origin_url = $suggested_origin_url;
            $origin_uri = $uri_factory->createUri($origin_url);
        }
        $success_return_uri = $cms_ui->decoratePageUri($entity_cms_admin_integration->getEditionFormUri($origin_uri));
        $error_return_uri = $cms_ui->decoratePageUri(
            $this->uri(
                'cms-ui-identity-component-password-edition-form',
                [
                    'component_address' => preg_replace('@^/@', '', $identity_component->getAddress()),
                ],
                [
                    'origin_url' => (string) $origin_uri,
                ]
            )
        );
        // Validate the nonce.
        $nonce = $this->getMainRequestBodyParam('nonce', '');
        $this->assertValidNonce('identity_entity_component_controller_process_entity_edition_form_' . $entity->getAddress(), $nonce);
        // Update the password.
        $html_error_messages = [];
        try {
            $password_1 = $this->getMainRequestBodyParam('password_1', '');
            $password_2 = $this->getMainRequestBodyParam('password_2', '');
            if ($password_1 !== $password_2) {
                $html_error_messages[] = 'Password and password confirmation don\'t match.';
                throw new Exception('Password and password confirmation don\'t match.');
            }
            $identity_component->setClearPassword($password_1);
            // Save the entity.
            $entity->save();
            $this->addHtmlFlashMessage(
                'success',
                'Password changed',
                '<p>' . htmlspecialchars('The password was successfully changed.') . '</p>'
            );
        } catch (Throwable $t) {
            $validation_errors = $identity_component->getValidationErrors();
            if (in_array(IdentityEntityComponent::VALIDATION_ERROR_EMPTY_PASSWORD, $validation_errors)) {
                $html_error_messages[] = 'Password cannot be empty.';
            }
            if (in_array(IdentityEntityComponent::VALIDATION_ERROR_PASSWORD_TOO_SHORT, $validation_errors)) {
                $html_error_messages[] = 'Password is too short.';
            }
            if (empty($html_error_messages)) {
                $html_body = '<p>' . htmlspecialchars($t->getMessage()) . '</p>';
            } else {
                $html_body = '<ul><li>' . implode('</li><li>', $html_error_messages) . '</li></ul>';
            }
            $this->addHtmlFlashMessage(
                'error',
                'Could not change password',
                $html_body
            );
            $this->seeOther($error_return_uri);
        }
        // Redirect the user.
        $this->seeOther($success_return_uri);
    }

}
