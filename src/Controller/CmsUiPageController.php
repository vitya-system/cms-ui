<?php

/*
 * Copyright 2021, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller;

use Psr\Http\Message\ResponseInterface;
use Vitya\Application\Controller\HtmlPageTrait;
use Vitya\CmsComponent\Controller\AbstractController;
use Vitya\CmsComponent\Entity\AbstractUserEntity;
use Vitya\Component\Session\HtmlFlashMessage;

class CmsUiPageController extends AbstractController
{
    use HtmlPageTrait;

    protected function createNonce(string $action, int $duration = 0): string
    {
        $nonce_manager = $this->getServiceFromHint('Vitya\CmsComponent\Nonce\NonceManagerInterface');
        if ($duration <= 0) {
            $duration = 1800;
        }
        return $nonce_manager->createNonce($action, $duration);
    }

    protected function assertValidNonce(string $action, string $token): void
    {
        $nonce_manager = $this->getServiceFromHint('Vitya\CmsComponent\Nonce\NonceManagerInterface');
        $nonce_manager->assertValidNonce($action, $token);
    }

    protected function render(string $template, array $parameters = [], ResponseInterface $response = null): ResponseInterface
    {
        $cms_ui = $this->getServiceFromHint('Vitya\CmsApplication\CmsUi');
        $flash_messages = [
            'success' => [],
            'error' => [],
            'warning' => [],
            'info' => [],
        ];
        foreach ($flash_messages as $queue => $v) {
            while ($message = $this->getFlashMessage($queue)) {
                if ($message instanceof HtmlFlashMessage) {
                    $flash_messages[$queue][] = [
                        'type' => 'html',
                        'html_title' => $message->getHtmlTitle(),
                        'html_body' => $message->getHtmlBody(),
                    ];
                } else {
                    $flash_messages[$queue][] = [
                        'type' => 'text',
                        'message' => $message->getMessage(),
                    ];
                }
            }
        }
        $cms_user = $this->getUser('cms');
        $cms_user_display_name = '';
        if (null !== $cms_user) {
            if ($cms_user instanceof AbstractUserEntity) {
                $cms_user_display_name = $cms_user->getTitle();
            } else {
                $cms_user_display_name = (string) $cms_user->getUserIdentifier();
            }
        }
        $parameters = array_merge(
            [
                'sidebar_widgets' => $cms_ui->getPageWidgets('sidebar'),
                'flash_messages' => $flash_messages,
                'cms_user_display_name' => $cms_user_display_name,
                'working_locale' => $cms_ui->getWorkingLocale(),
                'html_page_variables' => $this->getHtmlPageVariables(),
            ],
            $parameters
        );
        return parent::render($template, $parameters, $response);
    }

}
