<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsApplication\Controller;

use Embed\Embed;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Vitya\CmsComponent\Controller\AbstractController;
use Vitya\CmsComponent\EntityComponent\EmbedEntityComponent;

class EmbedEntityComponentController extends AbstractController
{
    public function scrape(string $component_address): ResponseInterface {
        $this->requireAuthentication('cms');
        // Is there a valid entity component?
        $embed_component = $this->ao()->getFromAddress('/' . $component_address);
        if (false === $embed_component instanceof EmbedEntityComponent) {
            $this->notFound();
        }
        // Is the user allowed to modify this entity component?
        if (false === $embed_component->canBeModified($this->getUser('cms'))) {
            $this->forbidden();
        }
        // Retrieve embed info.
        $url = trim($this->getMainRequestQueryParam('url', ''));
        if (false === filter_var($url, FILTER_VALIDATE_URL)) {
            throw new Exception('Invalid URL: "' . $url . '".');
        }
        $embed = new Embed();
        $embed_info = $embed->get($url);
        // Make a response.
        $a = [
            'status' => 'ok',
            'content' => [
                'title' => (string) $embed_info->title,
                'description' => (string) $embed_info->description,
                'url' => (string) $embed_info->url,
                'keywords' => implode("\n", $embed_info->keywords),
                'image' => (string) $embed_info->image,
                'html' => (string) $embed_info->code->html,
                'width' => (string) $embed_info->code->width,
                'height' => (string) $embed_info->code->height,
                'ratio' => (string) $embed_info->code->ratio,
                'author_name' => (string) $embed_info->authorName,
                'author_url' => (string) $embed_info->authorUrl,
                'cms' => (string) $embed_info->cms,
                'language' => (string) $embed_info->language,
                'languages' => implode("\n", $embed_info->languages),
                'provider_name' => (string) $embed_info->providerName,
                'provider_url' => (string) $embed_info->providerUrl,
                'icon' => (string) $embed_info->icon,
                'favicon' => (string) $embed_info->favicon,
                'published_time' => (string) $embed_info->publishedTime,
                'license' => (string) $embed_info->license,
                'feeds' => implode("\n", $embed_info->feeds),
            ],
        ];
        return $this->json($a);
    }

}
