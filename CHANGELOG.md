# Changelog

## [1.0.0-alpha14] - 2025-03-07
### Chaged
- Transform cms-ui into cms-application.

## [1.0.0-alpha12] - 2024-09-30
### Added
- Logger.

## [1.0.0-alpha11] - 2024-07-16
### Added
- Many untracked additions.

## [1.0.0-alpha10] - 2022-07-01
### Added
- Added a proper login page.
- Added nonces to the admin forms.
- Added a rudimentary LinkedEntitiesDatum edition widget.

### Changed
- Default user groups names have been changed.

## [1.0.0-alpha9] - 2022-06-03
### Changed
- Big refactoring.

## [1.0.0-alpha8] - 2021-08-02
### Changed
- Simplify data widgets handling.
- Handle non localized data.

## [1.0.0-alpha7] - 2021-07-05
### Changed
- Basic multilingual content edition.

## [1.0.0-alpha6] - 2021-06-07
### Changed
- Adapt to router reorganization.
- Data widget is now configurable.
### Fixed
- There is now a functional "back" link on creation forms.

## [1.0.0-alpha5] - 2021-05-03
### Added
- Proto data widget and subwidgets.
- Page UI widgets can pass and receive parameters to and from the URL query
  string.
- Save, delete and duplicate functionalities.
- The CMS UI retrieves and displays success / error / info flash messages.
### Changed
- Edition forms are processed for real.
- L10n widget is hidden when there is only one locale.
- Real SDBCU titles are displayed in the admin interface.

## [1.0.0-alpha4] - 2021-04-07
### Changed
- Bumped version to alpha4 to match the rest of the system.

## [1.0.0-alpha3] - 2021-03-01
### Changed
- Make the l10n widget fetch the available locales from the l10n service.

## [1.0.0-alpha2] - 2021-02-01
### Added
- Add a cms_ui service that helps to construct the admin interface.
- Add a proto localization widget.
### Changed
- Organize the UI using widgets.

## [1.0.0-alpha1] - 2021-01-04
### Added
- First release.
