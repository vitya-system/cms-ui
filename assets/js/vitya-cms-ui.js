// UI initialization.

const cmsUiInit = new Event('cmsUiInit');

window.addEventListener('load', (event) => {
    document.dispatchEvent(cmsUiInit);
});

// Edition form language switcher.

class CmsUiLocaleSwitcher {

    #localeSwitcherDomElement;

    constructor(localeSwitcherDomElement) {
        this.#localeSwitcherDomElement = localeSwitcherDomElement;
    }

    init() {
        if (false === this.#localeSwitcherDomElement.classList.contains('uninitialized')) {
            return;
        }
        this.#localeSwitcherDomElement.classList.remove('uninitialized');
        this.update();
        let that = this;
        document.addEventListener('cmsUiInit', function (e) {
            that.update();
        });
        let localeCheckboxes = this.#localeSwitcherDomElement.querySelectorAll('.language-switcher__checkbox');
        for (let localeCheckbox of localeCheckboxes) {
            localeCheckbox.addEventListener('change', function (e) {
                that.update();
            });
        }
    }

    update() {
        let localeCheckboxes = this.#localeSwitcherDomElement.querySelectorAll('.language-switcher__checkbox');
        let localeContainers = document.querySelectorAll('.locale-container');
        for (let localeCheckbox of localeCheckboxes) {
            for (let localeContainer of localeContainers) {
                if (localeContainer.dataset.locale === localeCheckbox.dataset.locale) {
                    if (localeCheckbox.checked) {
                        localeContainer.hidden = false;
                    } else {
                        localeContainer.hidden = true;
                    }
                }
            }
        }
    }

}

document.addEventListener('cmsUiInit', function (e) {
    let localeSwitcherDomElements = document.querySelectorAll('.language-switcher.uninitialized');
    for (let localeSwitcherDomElement of localeSwitcherDomElements) {
        let localeSwitcher = new CmsUiLocaleSwitcher(localeSwitcherDomElement);
        localeSwitcher.init();
    }
});

// Embed component widget.

document.addEventListener('cmsUiInit', function (e) {
    let embedEntityComponentWidgets = document.querySelectorAll('.embed-entity-component-widget.uninitialized');
    for (let embedEntityComponentWidget of embedEntityComponentWidgets) {
        embedEntityComponentWidget.classList.remove('uninitialized');
        embedEntityComponentWidget.addEventListener('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            if (event.target.classList.contains('embed-entity-component-widget__retrieve-info')) {
                event.target.classList.add('disabled');
                embedEntityComponentWidget.querySelector('.embed-entity-component-widget__spinner').classList.remove('d-none');
                fetch(event.target.getAttribute('href') + '?' + new URLSearchParams({
                        url: embedEntityComponentWidget.querySelector('.embed-entity-component-widget__source-url').value,
                    }).toString())
                    .then(response => response.json())
                    .then(data => {
                        if (data.status != 'ok') {
                            return;
                        }
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__title').value = data.content.title;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__description').value = data.content.description;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__url').value = data.content.url;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__keywords').value = data.content.keywords;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__image').value = data.content.image;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__html').value = data.content.html;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__width').value = data.content.width;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__height').value = data.content.height;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__ratio').value = data.content.ratio;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__author-name').value = data.content.author_name;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__author-url').value = data.content.author_url;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__cms').value = data.content.cms;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__language').value = data.content.language;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__languages').value = data.content.languages;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__provider-name').value = data.content.provider_name;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__provider-url').value = data.content.provider_url;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__icon').value = data.content.icon;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__favicon').value = data.content.favicon;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__published-time').value = data.content.published_time;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__license').value = data.content.license;
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__feeds').value = data.content.feeds;
                    })
                    .catch(err => console.log(err))
                    .finally(() => {
                        embedEntityComponentWidget.querySelector('.embed-entity-component-widget__spinner').classList.add('d-none');
                        event.target.classList.remove('disabled');
                    });
            }
        });
    }
});

// Tree index controller.

document.addEventListener('cmsUiInit', function (e) {
    let treeStructureWidgets = document.querySelectorAll('.tree-structure-widget.uninitialized');
    for (let treeStructureWidget of treeStructureWidgets) {
        treeStructureWidget.classList.remove('uninitialized');
        let tree = new SortableTree({
            nodes: JSON.parse(treeStructureWidget.querySelector('.tree-structure-widget__json').textContent),
            element: treeStructureWidget.querySelector('.tree-structure-widget__tree'),
            lockRootLevel: false,
            renderLabel: (data) => {
                return data.item_html;
            },
            onChange: ({nodes, movedNode, srcParentNode, targetParentNode}) => {
                let newAssociativeArray = [];
                let alreadyAddedNodes = [];
                Object.values(tree.nodeCollection).forEach(function(value, key) {
                    let parentId = parseInt(value._data.id);
                    if (!alreadyAddedNodes.includes(parentId)) {
                        newAssociativeArray.push([parentId, 0]);
                        alreadyAddedNodes.push(parentId);
                    }
                    value.subnodesData.forEach(function(subnodeData) {
                        let childId = parseInt(subnodeData.id);
                        if (!alreadyAddedNodes.includes(childId)) {
                            newAssociativeArray.push([childId, parentId]);
                            alreadyAddedNodes.push(childId);
                        }
                    });
                });
                treeStructureWidget.querySelector('.tree-structure-widget__associative-array').value = JSON.stringify(newAssociativeArray, 4);
            }
        });
    }
});

// CKEditor.

document.addEventListener('cmsUiInit', function (e) {
    let editionFormWidgets = document.querySelectorAll('.cke-edition-form-widget.uninitialized');
    for (let editionFormWidget of editionFormWidgets) {
        editionFormWidget.classList.remove('uninitialized');
        let editionFormWidgetTextarea = editionFormWidget.querySelector('textarea');
        ClassicEditor
            .create(
                editionFormWidgetTextarea, 
                {
                    toolbar: {
                        items: ['heading', '|', 'bold', 'italic', '|', 'undo', 'redo', '|', 'numberedList', 'bulletedList']
                    }
                }
            )
            .then(editor => {
                if (editionFormWidgetTextarea.disabled) {
                    editor.enableReadOnlyMode('datum-permission');
                }
            })
            .catch(error => {
                console.error(error.stack);
            })
        ;
    }
});

// Composite list.

document.addEventListener('cmsUiInit', function (e) {
    // Add block.
    let editionFormWidgets = document.querySelectorAll('.composite-list-edition-form-widget.uninitialized');
    for (let editionFormWidget of editionFormWidgets) {
        editionFormWidget.classList.remove('uninitialized');
        let editionFormWidgetAddBlockLinks = editionFormWidget.querySelector('.composite-list-edition-form-widget__add-block-links').querySelectorAll('.composite-list-edition-form-widget__add-block-link');
        for (let editionFormWidgetAddBlockLink of editionFormWidgetAddBlockLinks) {
            editionFormWidgetAddBlockLink.addEventListener('click', async function (e) {
                e.preventDefault();
                let response = await fetch(this.getAttribute('href'), {
                    method: 'GET'
                });
                if (response.ok) {
                    let template = document.createElement('template');
                    template.innerHTML = await response.text();
                    console.log(template.innerHTML);
                    if (template.content.childNodes.length >= 1) {
                        let newBlock = template.content.firstChild;
                        editionFormWidget.querySelector('.composite-list-edition-form-widget__block-list').appendChild(newBlock);
                        document.dispatchEvent(new Event('cmsUiInit'));
                    }
                }
            });
        }
        let editionFormWidgetBlockList = editionFormWidget.querySelector('.composite-list-edition-form-widget__block-list');
        new Sortable(editionFormWidgetBlockList, {
            handle: '.composite-list-edition-form-widget__block-handle',
            draggable: '.composite-list-edition-form-widget__block',
            animation: 150
        });
    }
    // Delete block.
    let editionFormWidgetBlocks = document.querySelectorAll('.composite-list-edition-form-widget__block.uninitialized');
    for (let editionFormWidgetBlock of editionFormWidgetBlocks) {
        editionFormWidgetBlock.classList.remove('uninitialized');
        let editionFormWidgetDelete = editionFormWidgetBlock.querySelector('.composite-list-edition-form-widget__delete');
        editionFormWidgetDelete.addEventListener('click', function (e) {
            editionFormWidgetBlock.parentNode.removeChild(editionFormWidgetBlock);
        });
    }
});
